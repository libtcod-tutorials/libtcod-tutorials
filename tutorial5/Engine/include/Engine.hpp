#ifndef ENGINE_HPP
#define ENGINE_HPP

#include "EntityManager.hpp"
#include "EventHandler.hpp"
#include "Map.hpp"
#include "Renderer.hpp"

#include <memory>

namespace tutorial
{
    struct Configuration;

    class Entity;

    class Engine
    {
    public:
        Engine(const Configuration& config);

        void ComputeFOV();
        EntityManager& GetEntityManager();
        EventHandler& GetEventHandler();
        void HandleEvents();
        void Render();
        void SetMap(Map&& map);
        void Quit();

        const EntityManager& GetEntityManager() const;
        Map* GetMap() const;
        Entity* GetPlayer() const;
        bool IsRunning() const;

    private:
        EventHandler event_handler_;

        EntityManager entities_;

        Renderer renderer_;

        std::unique_ptr<Map> map_;

        Entity* player_;

        bool running_;
    };
} // namespace tutorial

#endif // ENGINE_HPP
