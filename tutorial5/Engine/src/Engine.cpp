#include "Engine.hpp"

#include "AnEvent.hpp"
#include "Configuration.hpp"
#include "EngineEvent.hpp"
#include "Entity.hpp"
#include "EventHandler.hpp"
#include "GenerateMapEventHandler.hpp"
#include "Map.hpp"
#include "Position.hpp"
#include "Rectangle.hpp"
#include "Renderable.hpp"
#include "Renderer.hpp"
#include "SDLEventHandler.hpp"

#include <memory>
#include <utility>
#include <vector>

namespace tutorial
{
    Engine::Engine(const Configuration& config)
        : entities_(61),
          renderer_(config),
          map_(nullptr),
          player_(nullptr),
          running_(true)
    {
        event_handler_.AddHandler(GenerateMapEventHandler { *this });
        event_handler_.AddHandler(MoveEventHandler { *this });
        event_handler_.AddHandler(AttackEventHandler { *this });
        event_handler_.AddHandler(MessageEventHandler { *this });
        event_handler_.AddHandler(QuitEventHandler { *this });

        constexpr int ui_height = 5;

        map_ = std::make_unique<Map>(Rectangle {
            config.window_size.width, config.window_size.height - ui_height });

        GenerateMapEvent event {};
        event_handler_.AddEvent(std::make_unique<GenerateMapEvent>(event));
        event_handler_.Handle();
    }

    void Engine::ComputeFOV()
    {
        constexpr int fov_radius = 10;

        map_->ComputeFov(player_->GetPos(), fov_radius);
        map_->Update();
    }

    EntityManager& Engine::GetEntityManager()
    {
        return entities_;
    }

    EventHandler& Engine::GetEventHandler()
    {
        return event_handler_;
    }

    void Engine::HandleEvents()
    {
        auto event = SDLEventHandler::HandleInput();

        if (event)
        {
            event_handler_.AddEvent(std::move(event));
        }

        event_handler_.Handle();
    }

    void Engine::Render()
    {
        // Clear screen
        renderer_.Clear();

        // Render map
        const auto& console = map_->GetConsole();
        renderer_.Blit(console);

        // Render entities
        std::vector<std::pair<Position, Renderable>> renderables {};

        for (const auto& entity : entities_)
        {
            if (map_->IsInFov(entity.GetPos()))
            {
                const auto pos = entity.GetPos();
                const auto renderable = entity.GetRenderable();
                renderables.push_back({ pos, renderable });
            }
        }

        renderer_.Blit(renderables);

        // Render to screen
        renderer_.Render();
    }

    void Engine::SetMap(Map&& map)
    {
        map_ = std::make_unique<Map>(std::move(map));
        player_ = &(*entities_.begin());
    }

    void Engine::Quit()
    {
        running_ = false;
    }

    const EntityManager& Engine::GetEntityManager() const
    {
        return entities_;
    }

    Map* Engine::GetMap() const
    {
        return map_.get();
    }

    Entity* Engine::GetPlayer() const
    {
        return player_;
    }

    bool Engine::IsRunning() const
    {
        return running_;
    }
} // namespace tutorial
