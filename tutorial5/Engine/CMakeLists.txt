add_library(Engine STATIC
    src/Engine.cpp
)

target_include_directories(Engine
    PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include
)

target_link_libraries(Engine
    Entities Events Map Rendering
)
