#include "SDLEventHandler.hpp"

#include "AnEvent.hpp"
#include "EngineEvent.hpp"

#include <SDL_events.h>

#include <memory>

namespace tutorial
{
    std::unique_ptr<AnEvent> SDLEventHandler::HandleInput()
    {
        SDL_Event event {};

        SDL_WaitEvent(&event);

        std::unique_ptr<AnEvent> game_event = nullptr;

        switch (event.type)
        {
            case SDL_QUIT:
                game_event = std::make_unique<QuitEvent>();
                break;
            case SDL_KEYDOWN:
                game_event = HandleKeydownEvent(event.key.keysym.sym);
                break;
            default:
                break;
        }

        SDL_FlushEvent(SDL_KEYDOWN);

        return game_event;
    }

    std::unique_ptr<AnEvent> SDLEventHandler::HandleKeydownEvent(
        const SDL_Keycode& key)
    {
        std::unique_ptr<AnEvent> event = nullptr;

        switch (key)
        {
            case SDLK_UP:
            {
                event = std::make_unique<MoveEvent>(MoveDirection::UP);
                break;
            }
            case SDLK_DOWN:
            {
                event = std::make_unique<MoveEvent>(MoveDirection::DOWN);
                break;
            }
            case SDLK_LEFT:
            {
                event = std::make_unique<MoveEvent>(MoveDirection::LEFT);
                break;
            }
            case SDLK_RIGHT:
            {
                event = std::make_unique<MoveEvent>(MoveDirection::RIGHT);
                break;
            }
            case SDLK_RETURN:
            {
                event = std::make_unique<GenerateMapEvent>();
                break;
            }
            case SDLK_ESCAPE:
            {
                event = std::make_unique<QuitEvent>();
                break;
            }
        }

        return event;
    }
} // namespace tutorial
