#include "GenerateMapEventHandler.hpp"

#include "Colors.hpp"
#include "Engine.hpp"
#include "EntityManager.hpp"
#include "Map.hpp"
#include "MapGenerator.hpp"
#include "Position.hpp"
#include "Rectangle.hpp"
#include "Renderable.hpp"
#include "Room.hpp"

#include <libtcod/color.hpp>
#include <libtcod/mersenne.hpp>

#include <cstddef>
#include <utility>
#include <vector>

namespace tutorial
{
    class GenerateMapEvent;

    GenerateMapEventHandler::GenerateMapEventHandler(Engine& engine)
        : DefaultEventHandler(engine)
    {
    }

    void GenerateMapEventHandler::Handle(GenerateMapEvent&)
    {
        if (auto* map = engine_.GetMap())
        {
            this->GenerateMap(map->GetSize());
        }
    }

    void GenerateMapEventHandler::GenerateMap(Rectangle size)
    {
        auto& entities = engine_.GetEntityManager();
        entities.Clear();

        constexpr int max_rooms = 30;
        constexpr int min_room_size = 6;
        constexpr int max_room_size = 10;
        const GeneratorConfiguration config { size, max_rooms, min_room_size,
                                              max_room_size };

        auto map = Map::Generator::Generate(config);

        constexpr int max_monsters_per_room = 2;

        const auto& rooms = map.GetRooms();

        for (std::size_t i = 0; i < rooms.size(); ++i)
        {
            if (i == 0)
            {
                this->PlacePlayer(rooms[i]);
            }
            else
            {
                this->PlaceEntities(rooms[i], max_monsters_per_room);
            }
        }

        engine_.SetMap(std::move(map));
        engine_.ComputeFOV();
    }

    void GenerateMapEventHandler::PlaceEntities(Room room, int max_monsters)
    {
        auto* rand = TCODRandom::getInstance();

        const auto monster_count = rand->getInt(0, max_monsters);

        auto& entities = engine_.GetEntityManager();

        for (int i = 0; i < monster_count; ++i)
        {
            const auto inner = room.GetInner();
            const auto x = rand->getInt(inner[0].x, inner[inner.size() - 1].x);
            const auto y = rand->getInt(inner[0].y, inner[inner.size() - 1].y);

            if (!entities.Get(Position { x, y }))
            {
                if (rand->getFloat(0.0f, 1.0f) < 0.8f)
                {
                    entities.Add("orc", Position { x, y },
                                 Renderable { color::Green, 'o' });
                }
                else
                {
                    entities.Add("troll", Position { x, y },
                                 Renderable { color::LightGreen, 'T' });
                }
            }
        }
    }

    void GenerateMapEventHandler::PlacePlayer(Room room)
    {
        constexpr char icon = '@';

        auto& entities = engine_.GetEntityManager();

        entities.Add("player", room.GetCenter(),
                     Renderable { color::White, icon });
    }
} // namespace tutorial
