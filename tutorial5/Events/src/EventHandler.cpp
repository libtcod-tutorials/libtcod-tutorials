#include "EventHandler.hpp"

#include "AnEvent.hpp"
#include "Engine.hpp"
#include "EngineEvent.hpp"
#include "Entity.hpp"
#include "EntityManager.hpp"
#include "Map.hpp"
#include "Position.hpp"

#include <fmt/base.h>

#include <algorithm>
#include <format>
#include <memory>
#include <utility>

namespace tutorial
{
    void EventHandler::AddEvent(std::unique_ptr<AnEvent>&& event)
    {
        events_.push(std::move(event));
    }

    void EventHandler::Handle()
    {
        while (!events_.empty())
        {
            const auto& event = events_.front();

            std::for_each(handlers_.begin(), handlers_.end(),
                          [&event](const auto& handler)
                          { event->Handle(*handler); });

            events_.pop();
        }
    }

    QuitEventHandler::QuitEventHandler(Engine& engine)
        : DefaultEventHandler(engine)
    {
    }

    void QuitEventHandler::Handle(QuitEvent&)
    {
        engine_.Quit();
    }

    MoveEventHandler::MoveEventHandler(Engine& engine)
        : DefaultEventHandler(engine)
    {
    }

    void MoveEventHandler::Handle(MoveEvent& event)
    {
        auto* player = engine_.GetPlayer();
        auto pos = player->GetPos();

        switch (event.direction_)
        {
            case MoveDirection::UP:
            {
                --pos.y;
                break;
            }
            case MoveDirection::DOWN:
            {
                ++pos.y;
                break;
            }
            case MoveDirection::LEFT:
            {
                --pos.x;
                break;
            }
            case MoveDirection::RIGHT:
            {
                ++pos.x;
                break;
            }
        }

        if (this->IsEntity(pos))
        {
            auto* defender = this->GetEntityAtPos(pos);

            auto& event_handler = engine_.GetEventHandler();
            event_handler.AddEvent(
                std::make_unique<AttackEvent>(*player, *defender));
        }
        else if (!this->IsWall(pos))
        {
            player->SetPos(pos);
            engine_.ComputeFOV();
        }
    }

    Entity* MoveEventHandler::GetEntityAtPos(Position pos)
    {
        auto& entities = engine_.GetEntityManager();

        for (auto& entity : entities)
        {
            if (entity.GetPos() == pos)
            {
                return &entity;
            }
        }

        return nullptr;
    }

    bool MoveEventHandler::IsEntity(Position pos) const
    {
        const auto& entities = engine_.GetEntityManager();

        for (const auto& entity : entities)
        {
            if (entity.GetPos() == pos)
            {
                return true;
            }
        }

        return false;
    }

    bool MoveEventHandler::IsWall(Position pos) const
    {
        const auto* map = engine_.GetMap();

        return map->IsWall(pos);
    }

    AttackEventHandler::AttackEventHandler(Engine& engine)
        : DefaultEventHandler(engine)
    {
    }

    void AttackEventHandler::Handle(AttackEvent& event)
    {
        const auto& name = event.defender_.GetName();
        const auto message = std::format("You harmlessly attack the {}!", name);

        auto& event_handler = engine_.GetEventHandler();
        event_handler.AddEvent(std::make_unique<MessageEvent>(message));
    }

    void MessageEventHandler::Handle(MessageEvent& event)
    {
        fmt::println("{}", event.message_);
    }
} // namespace tutorial
