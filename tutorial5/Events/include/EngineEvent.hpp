#ifndef EVENT_HPP
#define EVENT_HPP

#include "AnEvent.hpp"

#include <string>

namespace tutorial
{
    class Entity;

    class GenerateMapEvent final : public HandleableEvent<GenerateMapEvent>
    {
    };

    class QuitEvent final : public HandleableEvent<QuitEvent>
    {
    };

    enum class MoveDirection : int
    {
        UP,
        DOWN,
        LEFT,
        RIGHT
    };

    class MoveEvent final : public HandleableEvent<MoveEvent>
    {
    public:
        MoveEvent(MoveDirection direction) : direction_(direction)
        {
        }

        MoveDirection direction_;
    };

    class AttackEvent final : public HandleableEvent<AttackEvent>
    {
    public:
        AttackEvent(Entity& attacker, Entity& defender)
            : attacker_(attacker), defender_(defender)
        {
        }

        Entity& attacker_;
        Entity& defender_;
    };

    class MessageEvent final : public HandleableEvent<MessageEvent>
    {
    public:
        MessageEvent(const std::string& message) : message_(message)
        {
        }

        std::string message_;
    };
} // namespace tutorial

#endif // EVENT_HPP
