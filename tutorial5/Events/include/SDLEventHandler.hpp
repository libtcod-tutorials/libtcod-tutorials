#ifndef SDL_EVENT_HANDLER_HPP
#define SDL_EVENT_HANDLER_HPP

#include <SDL_keycode.h>

#include <memory>

namespace tutorial
{
    class AnEvent;

    class SDLEventHandler
    {
    public:
        static std::unique_ptr<AnEvent> HandleInput();

    private:
        static std::unique_ptr<AnEvent> HandleKeydownEvent(const SDL_Keycode& key);
    };
} // namespace tutorial

#endif // SDL_EVENT_HANDLER_HPP
