#ifndef GENERATE_MAP_EVENT_HANDLER
#define GENERATE_MAP_EVENT_HANDLER

#include "DefaultEventHandler.hpp"

namespace tutorial
{
    struct Rectangle;

    class Engine;
    class GenerateMapEvent;
    class Room;

    class GenerateMapEventHandler final : public DefaultEventHandler
    {
    public:
        GenerateMapEventHandler(Engine& engine);

        using DefaultEventHandler::Handle;
        void Handle(GenerateMapEvent& event) override;

    private:
        void PlaceEntities(Room room, int max_monsters);
        void PlacePlayer(Room room);

        void GenerateMap(Rectangle size);
    };
} // namespace tutorial

#endif // GENERATE_MAP_EVENT_HANDLER
