#ifndef EVENT_HANDLER_HPP
#define EVENT_HANDLER_HPP

#include "AnEvent.hpp"
#include "DefaultEventHandler.hpp"

#include <memory>
#include <queue>
#include <vector>

namespace tutorial
{
    struct Position;

    class AttackEvent;
    class Engine;
    class Entity;
    class MessageEvent;
    class MoveEvent;
    class QuitEvent;

    class EventHandler
    {
    public:
        template <typename T>
        void AddHandler(T&& handler)
        {
            handlers_.push_back(std::make_unique<T>(handler));
        }

        void AddEvent(std::unique_ptr<AnEvent>&& event);
        void Handle();

    private:
        using EventHandler_UPtr = std::unique_ptr<DefaultEventHandler>;
        std::vector<EventHandler_UPtr> handlers_ {};

        using Event_UPtr = std::unique_ptr<AnEvent>;
        std::queue<Event_UPtr> events_;
    };

    class QuitEventHandler final : public DefaultEventHandler
    {
    public:
        QuitEventHandler(Engine& engine);

        using DefaultEventHandler::Handle;
        void Handle(QuitEvent& event) override;
    };

    class MoveEventHandler final : public DefaultEventHandler
    {
    public:
        MoveEventHandler(Engine& engine);

        using DefaultEventHandler::Handle;
        void Handle(MoveEvent& event) override;

    private:
        Entity* GetEntityAtPos(Position pos);

        bool IsEntity(Position pos) const;
        bool IsWall(Position pos) const;
    };

    class AttackEventHandler : public DefaultEventHandler
    {
    public:
        AttackEventHandler(Engine& engine);

        using DefaultEventHandler::Handle;
        void Handle(AttackEvent& event) override;
    };

    class MessageEventHandler : public DefaultEventHandler
    {
    public:
        MessageEventHandler(Engine& engine) : DefaultEventHandler(engine)
        {
        }

        using DefaultEventHandler::Handle;
        void Handle(MessageEvent& event) override;
    };
} // namespace tutorial

#endif // EVENT_HANDLER_HPP
