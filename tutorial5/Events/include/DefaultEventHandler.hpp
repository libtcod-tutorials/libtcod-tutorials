#ifndef DEFAULT_EVENT_HANDLER_HPP
#define DEFAULT_EVENT_HANDLER_HPP

#include "AnEventHandler.hpp"

namespace tutorial
{
    class AttackEvent;
    class Engine;
    class Entity;
    class GenerateMapEvent;
    class MoveEvent;
    class Room;
    class QuitEvent;

    class DefaultEventHandler
        : public MultiEventHandler<AnEvent, GenerateMapEvent, MoveEvent,
                                   AttackEvent, MessageEvent, QuitEvent>
    {
    public:
        DefaultEventHandler(Engine& engine) : engine_(engine)
        {
        }

    protected:
        Engine& engine_;
    };
} // namespace tutorial

#endif // DEFAULT_EVENT_HANDLER_HPP
