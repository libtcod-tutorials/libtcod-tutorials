#ifndef AN_EVENT_HANDLER_HPP
#define AN_EVENT_HANDLER_HPP

namespace tutorial
{
    class AnEvent;
    class AttackEvent;
    class GenerateMapEvent;
    class MessageEvent;
    class MoveEvent;
    class QuitEvent;

    class AnEventHandler
    {
    public:
        virtual ~AnEventHandler() = default;

        virtual void Handle(AnEvent&) = 0;
        virtual void Handle(GenerateMapEvent&) = 0;
        virtual void Handle(MoveEvent&) = 0;
        virtual void Handle(AttackEvent&) = 0;
        virtual void Handle(MessageEvent&) = 0;
        virtual void Handle(QuitEvent&) = 0;
    };

    template <typename T>
    class SingleEventHandler : virtual public AnEventHandler
    {
    public:
        using AnEventHandler::Handle;
        void Handle(T&) override
        {
        }
    };

    template <typename... T>
    class MultiEventHandler : public SingleEventHandler<T>...
    {
    public:
        using SingleEventHandler<T>::Handle...;
    };
} // namespace tutorial

#endif // AN_EVENT_HANDLER_HPP
