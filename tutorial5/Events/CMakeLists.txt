add_library(Events STATIC
    src/EventHandler.cpp
    src/GenerateMapEventHandler.cpp
    src/SDLEventHandler.cpp
)

target_include_directories(Events
    PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include
)

target_link_libraries(Events
    Engine
)
