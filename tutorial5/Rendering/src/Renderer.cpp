#include "Renderer.hpp"

#include "Configuration.hpp"
#include "Position.hpp"
#include "Rectangle.hpp"
#include "Renderable.hpp"

#include <SDL2/SDL_video.h>
#include <libtcod/version.h>
#include <libtcod/console.h>
#include <libtcod/console.hpp>
#include <libtcod/context.hpp>

namespace tutorial
{
    Renderer::Renderer(const Configuration& config)
        : console_(config.window_size.width, config.window_size.height)
    {
        TCOD_ContextParams params {};
        params.tcod_version = TCOD_COMPILEDVERSION;
        params.console = console_.get();
        params.window_title = config.window_title.data();
        params.sdl_window_flags = SDL_WINDOW_RESIZABLE;

        context_ = tcod::Context { params };
    }

    void Renderer::Blit(const tcod::Console& console)
    {
        tcod::blit(console_, console);
    }

    void Renderer::Blit(
        const std::vector<std::pair<Position, Renderable>>& renderables)
    {
        for (auto [pos, renderable] : renderables)
        {
            console_.at({ pos.x, pos.y }).ch = renderable.icon;
            console_.at({ pos.x, pos.y }).fg = renderable.color;
        }
    }

    void Renderer::Clear()
    {
        console_.clear();
    }

    void Renderer::Render()
    {
        context_.present(console_);
    }
} // namespace tutorial
