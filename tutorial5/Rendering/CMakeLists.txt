add_library(Rendering STATIC
    src/Renderer.cpp
)

target_include_directories(Rendering
    PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include
)

target_link_libraries(Rendering
    libtcod::libtcod
)
