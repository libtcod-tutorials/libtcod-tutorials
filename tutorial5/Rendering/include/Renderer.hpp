#ifndef RENDERER_HPP
#define RENDERER_HPP

#include <libtcod/console_types.hpp>
#include <libtcod/context.hpp>

#include <utility>
#include <vector>

namespace tutorial
{
    struct Configuration;
    struct Position;
    struct Renderable;

    class Renderer
    {
    public:
        Renderer(const Configuration& config);

        void Blit(const tcod::Console& console);
        void Blit(
            const std::vector<std::pair<Position, Renderable>>& renderables);
        void Clear();
        void Render();

    private:
        tcod::Console console_;
        tcod::Context context_;
    };
} // namespace tutorial

#endif // RENDERER_HPP
