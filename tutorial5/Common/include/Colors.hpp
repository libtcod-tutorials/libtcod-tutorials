#ifndef COLORS_HPP
#define COLORS_HPP

#include <libtcod/color.hpp>

namespace tutorial::color
{
    constexpr tcod::ColorRGB Black {};
    constexpr tcod::ColorRGB Yellow { 255, 255, 0 };
    constexpr tcod::ColorRGB White { 255, 255, 255 };
    constexpr tcod::ColorRGB LightAzure { 0, 95, 191 };
    constexpr tcod::ColorRGB DarkAzure { 63, 59, 155 };
    constexpr tcod::ColorRGB LightAmber { 255, 207, 63 };
    constexpr tcod::ColorRGB DarkAmber { 191, 143, 0 };
    constexpr tcod::ColorRGB Green { 63, 127, 63 };
    constexpr tcod::ColorRGB LightGreen { 0, 127, 0 };
} // namespace tutorial::color

#endif // COLORS_HPP
