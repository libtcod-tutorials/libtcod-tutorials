#ifndef RENDERABLE_HPP
#define RENDERABLE_HPP

#include <libtcod/color.hpp>

namespace tutorial
{
    struct Renderable
    {
        TCOD_ColorRGB color;
        char icon;
    };
} // namespace tutorial

#endif // RENDERABLE_HPP
