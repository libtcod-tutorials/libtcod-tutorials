#include "Entity.hpp"

#include "Renderable.hpp"

#include <string>
#include <string_view>

namespace tutorial
{
    Entity::Entity(const std::string& name, Position pos, Renderable renderable)
        : name_(name), pos_(pos), renderable_(renderable)
    {
    }

    std::string_view Entity::GetName() const
    {
        return name_;
    }

    void Entity::SetPos(Position pos)
    {
        pos_ = pos;
    }

    Position Entity::GetPos() const
    {
        return pos_;
    }

    Renderable Entity::GetRenderable() const
    {
        return renderable_;
    }
} // namespace tutorial
