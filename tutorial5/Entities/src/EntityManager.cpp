#include "EntityManager.hpp"

#include "Position.hpp"

namespace tutorial
{
    EntityManager::EntityManager(std::size_t size)
    {
        entities_.reserve(size);
    }

    void EntityManager::Clear()
    {
        entities_.clear();
    }

    Entity* EntityManager::Get(Position pos)
    {
        for (auto& entity : entities_)
        {
            if (entity.GetPos() == pos)
            {
                return &entity;
            }
        }

        return nullptr;
    }
} // namespace tutorial
