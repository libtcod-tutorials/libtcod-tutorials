#ifndef ENTITY_MANAGER_HPP
#define ENTITY_MANAGER_HPP

#include "Entity.hpp"

#include <cstddef>
#include <utility>
#include <vector>

namespace tutorial
{
    struct Position;

    class EntityManager
    {
    public:
        explicit EntityManager(std::size_t size = 0);

        template <typename... Args>
        void Add(Args&&... args);

        void Clear();
        Entity* Get(Position pos);

        std::vector<Entity>::iterator begin()
        {
            return entities_.begin();
        }

        std::vector<Entity>::iterator end()
        {
            return entities_.end();
        }

        std::vector<Entity>::const_iterator begin() const
        {
            return entities_.begin();
        }

        std::vector<Entity>::const_iterator end() const
        {
            return entities_.end();
        }

    private:
        std::vector<Entity> entities_ {};
    };

    template <typename... Args>
    void EntityManager::Add(Args&&... args)
    {
        entities_.emplace_back(std::forward<Args>(args)...);
    }
} // namespace tutorial

#endif // ENTITY_MANAGER_HPP
