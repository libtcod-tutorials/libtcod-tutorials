#ifndef ENTITY_HPP
#define ENTITY_HPP

#include "Position.hpp"
#include "Renderable.hpp"

#include <string>
#include <string_view>

namespace tutorial
{
    class Entity
    {
    public:
        Entity(const std::string& name, Position pos, Renderable renderable);

        void SetPos(Position pos);

        std::string_view GetName() const;
        Position GetPos() const;
        Renderable GetRenderable() const;

    private:
        std::string name_;
        Position pos_;
        Renderable renderable_;
    };
} // namespace tutorial

#endif // ENTITY_HPP
