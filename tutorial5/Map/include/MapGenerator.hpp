#ifndef MAP_GENERATOR_HPP
#define MAP_GENERATOR_HPP

#include "Map.hpp"
#include "Rectangle.hpp"

namespace tutorial
{
    struct GeneratorConfiguration
    {
        Rectangle size;
        int max_rooms;
        int min_room_size;
        int max_room_size;
    };

    class Map::Generator
    {
    public:
        static Map Generate(const GeneratorConfiguration config);
    };
} // namespace tutorial

#endif // MAP_GENERATOR_HPP
