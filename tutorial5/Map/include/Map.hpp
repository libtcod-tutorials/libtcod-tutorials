#ifndef MAP_HPP
#define MAP_HPP

#include "Rectangle.hpp"
#include "Room.hpp"
#include "Tile.hpp"

#include <libtcod/console_types.hpp>
#include <libtcod/fov.hpp>

#include <memory>
#include <vector>

namespace tutorial
{
    struct Position;

    class Map
    {
    public:
        class Generator;

        explicit Map(Rectangle size);

        void ComputeFov(Position origin, int fovRadius);
        void SetExplored(Position pos, bool explored);
        void SetTileType(Position pos, TileType type);
        void Update();

        const tcod::Console& GetConsole() const;
        const std::vector<Room>& GetRooms() const;
        Rectangle GetSize() const;
        TileType GetTileType(Position pos) const;
        bool IsExplored(Position pos) const;
        bool IsInBounds(Position pos) const;
        bool IsInFov(Position pos) const;
        bool IsWall(Position pos) const;

    private:
        std::vector<Room> rooms_;
        std::vector<tile_t> tiles_;

        std::unique_ptr<TCODMap> map_;
        tcod::Console console_;

        Rectangle size_;
    };
} // namespace tutorial

#endif // MAP_HPP
