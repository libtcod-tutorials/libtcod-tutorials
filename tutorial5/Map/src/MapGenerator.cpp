#include "MapGenerator.hpp"

#include "Position.hpp"
#include "Rectangle.hpp"
#include "Room.hpp"
#include "Tile.hpp"

#include <libtcod/bresenham.hpp>
#include <libtcod/mersenne.hpp>

#include <algorithm>
#include <vector>

namespace tutorial
{
    std::vector<Position> tunnelBetween(Position start, Position end)
    {
        auto* rand = TCODRandom::getInstance();

        Position corner { 0, 0 };

        constexpr float half_chance = 0.5F;

        if (rand->get(0.0F, 1.0F) < half_chance)
        {
            corner = Position { end.x, start.y };
        }
        else
        {
            corner = Position { start.x, end.y };
        }

        std::vector<Position> tunnel {};

        for (const auto&& [x, y] :
             tcod::BresenhamLine({ start.x, start.y }, { corner.x, corner.y }))
        {
            tunnel.push_back({ x, y });
        }

        for (const auto&& [x, y] :
             tcod::BresenhamLine({ corner.x, corner.y }, { end.x, end.y }))
        {
            tunnel.push_back({ x, y });
        }

        return tunnel;
    }

    Map Map::Generator::Generate(const GeneratorConfiguration config)
    {
        const auto size = config.size;

        Map map { size };

        auto* rand = TCODRandom::getInstance();

        for (int i = 0; i < config.max_rooms; ++i)
        {
            int roomWidth =
                rand->getInt(config.min_room_size, config.max_room_size);
            int roomHeight =
                rand->getInt(config.min_room_size, config.max_room_size);

            Position roomOrigin { rand->getInt(0, size.width - roomWidth - 1),
                                  rand->getInt(0,
                                               size.height - roomHeight - 1) };

            auto room = Room(roomOrigin, roomWidth, roomHeight);

            auto intersects = [&room](Room other)
            { return room.Intersects(other); };

            auto it =
                std::find_if(map.rooms_.begin(), map.rooms_.end(), intersects);

            if (it != map.rooms_.end())
            {
                continue;
            }

            for (auto pos : room.GetInner())
            {
                if (map.IsWall(pos))
                {
                    map.SetTileType(pos, TileType::FLOOR);
                }
            }

            if (!map.rooms_.empty())
            {
                auto tunnel = tunnelBetween(
                    room.GetCenter(),
                    map.rooms_.at(map.rooms_.size() - 1).GetCenter());

                for (auto pos : tunnel)
                {
                    if (map.IsWall(pos))
                    {
                        map.SetTileType(pos, TileType::FLOOR);
                    }
                }
            }

            map.rooms_.push_back(room);
        }

        return map;
    }
} // namespace tutorial
