#include "Room.hpp"

namespace tutorial
{
    Room::Room(Position origin, int width, int height)
        : origin_(origin),
          end_(Position { origin.x + width, origin.y + height })
    {
    }

    Position Room::GetCenter() const
    {
        return ((origin_ + end_) / 2);
    }

    std::vector<Position> Room::GetInner() const
    {
        std::vector<Position> inner;

        for (int x = origin_.x + 1; x < end_.x; ++x)
        {
            for (int y = origin_.y + 1; y < end_.y; ++y)
            {
                inner.push_back(Position { x, y });
            }
        }

        return inner;
    }

    bool Room::Intersects(Room& other) const
    {
        return (origin_.x <= other.end_.x && end_.x >= other.origin_.x
                && origin_.y <= other.end_.y && end_.y >= other.origin_.y);
    }
} // namespace tutorial
