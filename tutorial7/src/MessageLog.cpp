#include "MessageLog.hpp"

#include <libtcod/console.hpp>

namespace tutorial
{
    void MessageLog::AddMessage(const std::string& text, TCODColor color,
                                bool stack)
    {
        if (stack && messages_.size() > 0 && text == messages_[-1].text)
        {
            messages_[-1].count += 1;
        }
        else
        {
            messages_.emplace_back(text, color);
        }
    }

    void MessageLog::Clear()
    {
        messages_.clear();
    }
} // namespace tutorial
