#include "AiComponent.hpp"

#include "Engine.hpp"
#include "Entity.hpp"
#include "Event.hpp"

#include <libtcod/bresenham.hpp>

#include <array>
#include <numeric>

namespace tutorial
{
    auto bresenhamLength = [](const tcod::BresenhamLine& path) -> std::size_t
    {
        std::size_t length = 0;

        for (auto it = path.begin(); it != path.end(); ++it)
        {
            ++length;
        }

        return length;
    };

    auto canPathToTarget = [](const tcod::BresenhamLine& path,
                              const Engine& engine) -> bool
    {
        for (const auto [x, y] : path)
        {
            if (engine.IsBlocker({ x, y }))
            {
                return false;
            }
        }

        return true;
    };

    auto checkCardinalPoints = [](pos_t pos,
                                  pos_t target) -> tcod::BresenhamLine
    {
        constexpr std::array<pos_t, 4> cardinals { {
            { 0, -1 }, // up
            { 0, 1 },  // down
            { -1, 0 }, // left
            { 1, 0 }   // right
        } };

        std::vector<tcod::BresenhamLine> lines;
        lines.reserve(4);

        for (auto [x, y] : cardinals)
        {
            auto new_pos = pos_t { x, y } + target;

            auto line =
                tcod::BresenhamLine({ pos.x, pos.y }, { new_pos.x, new_pos.y })
                    .without_start();

            lines.push_back(line);
        }

        auto shortest = lines[0];

        for (auto line : lines)
        {
            if (bresenhamLength(shortest) > bresenhamLength(line))
            {
                shortest = line;
            }
        }

        return shortest;
    };

    BaseAi::BaseAi(Entity& entity) : entity_(entity)
    {
    }

    void BaseAi::Perform(Engine&)
    {
        // No op
    }

    HostileAi::HostileAi(Entity& entity) : BaseAi(entity)
    {
    }

    void HostileAi::Perform(Engine& engine)
    {
        auto pos = entity_.GetPos();

        if (engine.IsInFov(pos))
        {
            auto target = engine.GetPlayer();
            auto targetPos = target->GetPos();
            auto delta = targetPos - pos;

            auto distance = std::max(std::abs(delta.x), std::abs(delta.y));
            auto is_diagonal = (std::abs(delta.x) + std::abs(delta.y)) > 1;

            if (distance == 1 && !is_diagonal)
            {
                auto action = MeleeAction(engine, entity_, delta);
                action.Execute();

                return;
            }

            auto path = checkCardinalPoints(pos, targetPos);

            if (canPathToTarget(path, engine))
            {
                auto dest = path[0];
                auto destPos = pos_t { dest[0], dest[1] } - pos;

                auto action = MoveAction(engine, entity_, destPos);
                action.Execute();

                return;
            }

            auto action = WaitAction(engine, entity_);
            action.Execute();
        }
    }
} // namespace tutorial
