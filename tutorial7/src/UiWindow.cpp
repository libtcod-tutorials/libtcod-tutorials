#include "UiWindow.hpp"

#include <libtcod/console.hpp>

namespace tutorial
{
    UiWindowBase::UiWindowBase(std::size_t width, std::size_t height, pos_t pos)
        : console_(new TCODConsole(width, height)), pos_(pos)
    {
    }

    void UiWindowBase::Render(TCODConsole*) const
    {
        // No op
    }
} // namespace tutorial
