#include "Engine.hpp"

#include "Colors.hpp"
#include "Entity.hpp"
#include "EntityManager.hpp"
#include "Event.hpp"
#include "EventHandler.hpp"
#include "HealthBar.hpp"
#include "Map.hpp"
#include "MapGenerator.hpp"
#include "MessageHistoryWindow.hpp"
#include "MessageLogWindow.hpp"

#include <libtcod/console.hpp>
#include <libtcod/sys.hpp>

#include <memory>
#include <string>

namespace tutorial
{
    constexpr int UI_HEIGHT = 5;
    constexpr int MAX_FPS = 60;

    constexpr int ROOM_MIN_SIZE = 6;
    constexpr int ROOM_MAX_SIZE = 10;
    constexpr int MAX_ROOMS = 30;

    constexpr int FOV_RADIUS = 10;

    constexpr int MAX_MONSTERS_PER_ROOM = 3;

    // Public methods
    Engine::Engine(std::size_t width, std::size_t height, std::string title)
        : eventHandler_(new MainGameEventHandler(*this)),
          map_(nullptr),
          messageHistoryWindow_(
              new MessageHistoryWindow(width, height, { 0, 0 }, messageLog_)),
          messageLogWindow_(
              new MessageLogWindow(40, 5, { 21, 45 }, messageLog_)),
          player_(nullptr),
          healthBar_(nullptr),
          width_(width),
          height_(height),
          window_(MainGame),
          running_(true)
    {
        // Initialize the tcod root console
        TCODConsole::initRoot(width, height, title.c_str());
        TCODSystem::setFps(MAX_FPS);

        NewGame();
    }

    Engine::~Engine()
    {
        // Need to call this before we exit to clean tcod up
        TCOD_quit();
    }

    void Engine::ComputeFOV()
    {
        map_->ComputeFov(player_->GetPos(), FOV_RADIUS);
        map_->Update();
    }

    Entity* Engine::GetBlockingEntity(pos_t pos)
    {
        return entities_.GetBlockingEntity(pos);
    }

    Entity* Engine::GetPlayer()
    {
        return player_;
    }

    void Engine::HandleDeathEvent()
    {
        eventHandler_ = std::make_unique<GameOverEventHandler>(*this);
    }

    void Engine::HandleEvents()
    {
        auto action = eventHandler_->Dispatch();

        if (action)
        {
            action->Execute();

            if (window_ == MainGame)
            {
                HandleEnemyTurns();
            }
        }
    }

    void Engine::LogMessage(const std::string& text, TCODColor color,
                            bool stack)
    {
        messageLog_.AddMessage(text, color, stack);
    }

    void Engine::NewGame()
    {
        messageLog_.Clear();

        GenerateMap(width_, height_ - UI_HEIGHT);

        // Place monsters
        auto rooms = map_->GetRooms();

        for (auto it = rooms.begin() + 1; it != rooms.end(); ++it)
        {
            entities_.PlaceEntities(*it, MAX_MONSTERS_PER_ROOM);
        }

        // Create player and add them to entity list
        player_ = &entities_.Spawn(PLAYER, rooms[0].GetCenter());

        // Create health bar
        healthBar_ = std::make_unique<HealthBar>(20, 1, pos_t { 0, 45 },
                                                 player_->GetDefense());

        ComputeFOV();

        messageLog_.AddMessage("Hello and welcome to the C++ libtcod dungeon!",
                               color::light_azure, false);

        window_ = MainGame;
    }

    void Engine::ReturnToMainGame()
    {
        if (window_ != MainGame)
        {
            eventHandler_ = std::make_unique<MainGameEventHandler>(*this);
            window_ = MainGame;
        }
    }

    void Engine::ShowMessageHistory()
    {
        if (window_ != MessageHistory)
        {
            eventHandler_ = std::make_unique<MessageHistoryEventHandler>(*this);
            window_ = MessageHistory;
        }
    }

    void Engine::Quit()
    {
        std::exit(0);
    }

    bool Engine::IsBlocker(pos_t pos) const
    {
        for (const auto& entity : entities_)
        {
            if (entity.IsBlocker() && entity.GetPos() == pos)
            {
                return true;
            }
        }

        return IsWall(pos);
    }

    bool Engine::IsInBounds(pos_t pos) const
    {
        return map_->IsInBounds(pos);
    }

    bool Engine::IsInFov(pos_t pos) const
    {
        return map_->IsInFov(pos);
    }

    bool Engine::IsPlayer(const Entity& entity) const
    {
        return player_ == &entity;
    }

    bool Engine::IsRunning() const
    {
        return (!TCODConsole::isWindowClosed() && running_);
    }

    bool Engine::IsWall(pos_t pos) const
    {
        return map_->IsWall(pos);
    }

    void Engine::Render() const
    {
        auto* root = TCODConsole::root;

        // Clear the console
        root->clear();

        if (window_ == MainGame)
        {
            // Render the map
            map_->Render(root);

            // Show all entitys positions if in fov
            for (const auto& entity : entities_)
            {
                if (map_->IsInFov(entity.GetPos()))
                {
                    entity.Render(root);
                }
            }

            // Render UI elements on top
            healthBar_->Render(root);
            messageLogWindow_->Render(root);
        }
        else if (window_ == MessageHistory)
        {
            messageHistoryWindow_->Render(root);
        }

        // Flushing the console redraws it to the screen
        TCODConsole::flush();
    }

    // Private methods
    void Engine::GenerateMap(int width, int height)
    {
        eventHandler_ = std::make_unique<MainGameEventHandler>(*this);

        entities_.Clear();

        Map::Generator generator(
            { MAX_ROOMS, ROOM_MIN_SIZE, ROOM_MAX_SIZE, width, height });

        map_ = generator.Generate();
        map_->Update();
    }

    void Engine::HandleEnemyTurns()
    {
        for (auto& entity : entities_)
        {
            if (IsPlayer(entity) || entity.IsDead())
            {
                continue;
            }

            entity.Act(*this);
        }
    }
} // namespace tutorial
