#include "HealthBar.hpp"

#include "Colors.hpp"

#include <libtcod/console.hpp>

namespace tutorial
{
    HealthBar::HealthBar(unsigned int width, unsigned int height, pos_t pos,
                         const DefenseComponent& health)
        : UiWindowBase(width, height, pos), health_(health)
    {
        console_->setDefaultBackground(color::dark_red);
        console_->rect(0, 0, console_->getWidth(), console_->getHeight(), true);
    }

    void HealthBar::Render(TCODConsole* parent) const
    {
        console_->clear();

        const auto width =
            (int)((float)(health_.hp) / health_.maxHp * console_->getWidth());

        if (width > 0)
        {
            for (int i = 0; i < width; ++i)
            {
                console_->setCharBackground(i, console_->getHeight() - 1,
                                            color::green);
            }
        }

        console_->printf(1, 0, "HP: %i/%i", health_.hp, health_.maxHp);

        TCODConsole::blit(console_.get(), 0, 0, console_->getWidth(),
                          console_->getHeight(), parent, pos_.x, pos_.y);
    }
} // namespace tutorial
