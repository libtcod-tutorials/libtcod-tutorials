#include "Event.hpp"
#include <string>

#include "Colors.hpp"
#include "Engine.hpp"
#include "Entity.hpp"
#include "Util.hpp"

namespace tutorial
{
    // Events
    MessageHistoryEvent::MessageHistoryEvent(Engine& engine)
        : EngineEvent(engine)
    {
    }

    void MessageHistoryEvent::Execute()
    {
        engine_.ShowMessageHistory();
    }

    NewGameEvent::NewGameEvent(Engine& engine) : EngineEvent(engine)
    {
    }

    void NewGameEvent::Execute()
    {
        engine_.NewGame();
    }

    ReturnToGameEvent::ReturnToGameEvent(Engine& engine) : EngineEvent(engine)
    {
    }

    void ReturnToGameEvent::Execute()
    {
        engine_.ReturnToMainGame();
    }

    QuitEvent::QuitEvent(Engine& engine) : EngineEvent(engine)
    {
    }

    void QuitEvent::Execute()
    {
        engine_.Quit();
    }

    // Actions
    DieAction::DieAction(Engine& engine, Entity& entity)
        : Action(engine, entity)
    {
    }

    void DieAction::Execute()
    {
        if (engine_.IsPlayer(entity_))
        {
            engine_.LogMessage("You died!", color::dark_red, false);
            engine_.HandleDeathEvent();
        }
        else
        {
            engine_.LogMessage(
                util::capitalize(entity_.GetName()) + " has died!",
                color::dark_red, true);
        }

        entity_.Die();
    }

    WaitAction::WaitAction(Engine& engine, Entity& entity)
        : Action(engine, entity)
    {
    }

    void WaitAction::Execute()
    {
        // No op
    }

    BumpAction::BumpAction(Engine& engine, Entity& entity, pos_t pos)
        : DirectionalAction(engine, entity, pos)
    {
    }

    void BumpAction::Execute()
    {
        if (entity_.IsDead())
        {
            return;
        }

        auto targetPos = entity_.GetPos() + pos_;

        if (engine_.GetBlockingEntity(targetPos))
        {
            auto action = MeleeAction(engine_, entity_, pos_);
            action.Execute();
        }
        else
        {
            auto action = MoveAction(engine_, entity_, pos_);
            action.Execute();
        }
    }

    MeleeAction::MeleeAction(Engine& engine, Entity& entity, pos_t pos)
        : DirectionalAction(engine, entity, pos)
    {
    }

    void MeleeAction::Execute()
    {
        auto targetPos = entity_.GetPos() + pos_;
        auto* target = engine_.GetBlockingEntity(targetPos);

        if (target && !target->IsDead())
        {
            auto damage = entity_.Attack() - target->Defend();

            if (damage > 0)
            {
                target->TakeDamage(damage);

                engine_.LogMessage(util::capitalize(entity_.GetName())
                                       + " attacks " + target->GetName()
                                       + " for " + std::to_string(damage)
                                       + " hit points.",
                                   color::red, true);

                if (target->IsDead())
                {
                    auto action = DieAction(engine_, *target);
                    action.Execute();
                }
            }
            else
            {
                engine_.LogMessage(util::capitalize(entity_.GetName())
                                       + " attacks " + target->GetName()
                                       + " but does no damage.",
                                   color::red, true);
            }
        }
    }

    MoveAction::MoveAction(Engine& engine, Entity& entity, pos_t pos)
        : DirectionalAction(engine, entity, pos)
    {
    }

    void MoveAction::Execute()
    {
        auto targetPos = entity_.GetPos() + pos_;

        if (engine_.IsInBounds(targetPos) && !engine_.IsWall(targetPos))
        {
            entity_.SetPos(targetPos);

            if (engine_.IsPlayer(entity_))
            {
                engine_.ComputeFOV();
            }
        }
    }
} // namespace tutorial
