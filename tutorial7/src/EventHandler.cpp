#include "EventHandler.hpp"

#include "Engine.hpp"
#include "Event.hpp"

#include "libtcod/sys.hpp"

namespace tutorial
{
    std::unique_ptr<Event> MainGameEventHandler::Dispatch() const
    {
        // Check for event sees if anything is in the input event queue and
        // If no event, return
        TCOD_key_t key {};

        TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS, &key, nullptr);

        std::unique_ptr<Event> event { nullptr };

        switch (key.vk)
        {
            case TCODK_UP:
                event = std::make_unique<BumpAction>(
                    engine_, *engine_.GetPlayer(), pos_t { 0, -1 });
                break;
            case TCODK_DOWN:
                event = std::make_unique<BumpAction>(
                    engine_, *engine_.GetPlayer(), pos_t { 0, 1 });
                break;
            case TCODK_LEFT:
                event = std::make_unique<BumpAction>(
                    engine_, *engine_.GetPlayer(), pos_t { -1, 0 });
                break;
            case TCODK_RIGHT:
                event = std::make_unique<BumpAction>(
                    engine_, *engine_.GetPlayer(), pos_t { 1, 0 });
                break;
            case TCODK_SPACE:
                event =
                    std::make_unique<WaitAction>(engine_, *engine_.GetPlayer());
                break;
            case TCODK_CHAR:
                if (key.c == 'v')
                {
                    event = std::make_unique<MessageHistoryEvent>(engine_);
                }
                break;
            case TCODK_ENTER:
                event = std::make_unique<NewGameEvent>(engine_);
                break;
            case TCODK_ESCAPE:
                event = std::make_unique<QuitEvent>(engine_);
                break;
            case TCODK_NONE:
            default:
                break;
        }

        return event;
    }

    std::unique_ptr<Event> MessageHistoryEventHandler::Dispatch() const
    {
        // Check for event sees if anything is in the input event queue and
        // If no event, return
        TCOD_key_t key {};

        TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS, &key, nullptr);

        std::unique_ptr<Event> event { nullptr };

        switch (key.vk)
        {
            case TCODK_CHAR:
                if (key.c == 'v')
                {
                    event = std::make_unique<ReturnToGameEvent>(engine_);
                }
                break;
            case TCODK_NONE:
            default:
                break;
        }

        return event;
    }

    std::unique_ptr<Event> GameOverEventHandler::Dispatch() const
    {
        // Check for event sees if anything is in the input event queue and
        // If no event, return
        TCOD_key_t key {};

        TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS, &key, nullptr);

        std::unique_ptr<Event> event { nullptr };

        switch (key.vk)
        {
            case TCODK_ENTER:
                event = std::make_unique<NewGameEvent>(engine_);
                break;
            case TCODK_ESCAPE:
                event = std::make_unique<QuitEvent>(engine_);
                break;
            case TCODK_NONE:
            default:
                break;
        }

        return event;
    }
} // namespace tutorial
