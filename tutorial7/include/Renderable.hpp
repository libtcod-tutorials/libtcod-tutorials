#ifndef RENDERABLE_COMPONNENT_HPP
#define RENDERABLE_COMPONNENT_HPP

#include "Position.hpp"

#include "libtcod/console.hpp"

namespace tutorial
{
    class RenderableComponent
    {
    public:
        virtual void Render(TCODConsole* console, pos_t pos) const = 0;
    };

    class CharRenderable : public RenderableComponent
    {
    public:
        CharRenderable(TCODColor color, char icon) : color_(color), icon_(icon)
        {
        }

        void Render(TCODConsole* console, pos_t pos) const override;

    private:
        TCODColor color_;
        char icon_;
    };
} // namespace tutorial

#endif // RENDERABLE_COMPONNENT_HPP
