#ifndef HEALTH_BAR_HPP
#define HEALTH_BAR_HPP

#include "Components.hpp"
#include "UiWindow.hpp"

class TCODConsole;

namespace tutorial
{
    class HealthBar : public UiWindowBase
    {
    public:
        HealthBar(unsigned int width, unsigned int height, pos_t pos,
                  const DefenseComponent& health);

        void Render(TCODConsole* parent) const override;

    private:
        const DefenseComponent& health_;
    };
} // namespace tutorial

#endif // HEALTH_BAR_HPP
