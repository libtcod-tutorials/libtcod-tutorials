#ifndef ENGINE_HPP
#define ENGINE_HPP

#include "EntityManager.hpp"
#include "MessageLog.hpp"
#include "Position.hpp"

#include <cstddef>
#include <memory>
#include <string>
#include <vector>

namespace tutorial
{
    enum Window
    {
        MainGame,
        MessageHistory
    };

    class Entity;
    class EventHandler;
    class HealthBar;
    class Map;
    class MessageHistoryWindow;
    class MessageLogWindow;

    class Engine
    {
    public:
        explicit Engine(std::size_t width, std::size_t height,
                        std::string title);
        ~Engine();

        void ComputeFOV();
        Entity* GetBlockingEntity(pos_t pos);
        Entity* GetPlayer();
        void HandleDeathEvent();
        void HandleEvents();
        void LogMessage(const std::string& text, TCODColor color, bool stack);
        void NewGame();
        void ReturnToMainGame();
        void ShowMessageHistory();
        void Quit();

        bool IsBlocker(pos_t pos) const;
        bool IsInBounds(pos_t pos) const;
        bool IsInFov(pos_t pos) const;
        bool IsPlayer(const Entity& entity) const;
        bool IsRunning() const;
        bool IsWall(pos_t pos) const;
        void Render() const;

    private:
        void GenerateMap(int width, int height);
        void HandleEnemyTurns();

        EntityManager entities_;
        MessageLog messageLog_;
        
        std::unique_ptr<EventHandler> eventHandler_;
        std::unique_ptr<Map> map_;
        std::unique_ptr<MessageHistoryWindow> messageHistoryWindow_;
        std::unique_ptr<MessageLogWindow> messageLogWindow_;

        Entity* player_;
        std::unique_ptr<HealthBar> healthBar_;

        int width_;
        int height_;
        Window window_;
        bool running_;
    };
} // namespace tutorial

#endif // ENGINE_HPP
