#ifndef COMPONENTS_HPP
#define COMPONENTS_HPP

#include "libtcod/color.hpp"

namespace tutorial
{
    struct AttackComponent
    {
        unsigned int power;
    };

    struct DefenseComponent
    {
        unsigned int defense;
        int hp;
        unsigned int maxHp;
    };
} // namespace tutorial

#endif // COMPONENTS_HPP
