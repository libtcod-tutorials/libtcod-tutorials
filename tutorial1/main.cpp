#include "Engine.hpp"

int main()
{
    tutorial::Engine engine {};

    while (engine.IsRunning())
    {
        engine.Render();
        engine.Input();
        engine.Update();
    }

    return 0;
}
