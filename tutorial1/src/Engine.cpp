#include "Engine.hpp"

#include "Event.hpp"
#include "Position.hpp"

#include <SDL2/SDL_events.h>
#include <SDL2/SDL_keycode.h>
#include <SDL2/SDL_video.h>
#include <libtcod/context.h>
#include <libtcod/version.h>
#include <libtcod/context.hpp>

#include <string_view>

namespace tutorial
{
    Engine::Engine() : running_(true)
    {
        constexpr int window_width = 80;
        constexpr int window_height = 50;
        constexpr std::string_view window_title = "libtcod C++ tutorial 1";

        console_ = tcod::Console { window_width, window_height };

        TCOD_ContextParams params {};
        params.tcod_version = TCOD_COMPILEDVERSION;
        params.console = console_.get();
        params.window_title = window_title.data();
        params.sdl_window_flags = SDL_WINDOW_RESIZABLE;

        context_ = tcod::Context { params };

        player_pos_ = Position { window_width / 2, window_height / 2 };
    }

    void Engine::Input()
    {
        SDL_Event event {};

        SDL_WaitEvent(&event);

        switch (event.type)
        {
            case SDL_QUIT:
                next_event_ = Event::Quit;
                break;
            case SDL_KEYDOWN:
                HandleKeydownEvent(event);
                break;
            default:
                next_event_ = Event::None;
                break;
        }

        SDL_FlushEvent(SDL_KEYDOWN);
    }

    void Engine::Render()
    {
        console_.clear();
        console_.at({ player_pos_.x, player_pos_.y }).ch = '@';

        context_.present(console_);
    }

    void Engine::Update()
    {
        auto new_pos = player_pos_;

        switch (next_event_)
        {
            case Event::MoveUp:
                --new_pos.y;
                break;
            case Event::MoveDown:
                ++new_pos.y;
                break;
            case Event::MoveLeft:
                --new_pos.x;
                break;
            case Event::MoveRight:
                ++new_pos.x;
                break;
            case Event::Quit:
                Quit();
                break;
            default:
                break;
        }

        if (console_.in_bounds({ new_pos.x, new_pos.y }))
        {
            player_pos_ = new_pos;
        }
    }

    bool Engine::IsRunning() const
    {
        return running_;
    }

    void Engine::HandleKeydownEvent(const SDL_Event& event)
    {
        switch (event.key.keysym.sym)
        {
            case SDLK_UP:
                next_event_ = Event::MoveUp;
                break;
            case SDLK_DOWN:
                next_event_ = Event::MoveDown;
                break;
            case SDLK_LEFT:
                next_event_ = Event::MoveLeft;
                break;
            case SDLK_RIGHT:
                next_event_ = Event::MoveRight;
                break;
            case SDLK_ESCAPE:
                next_event_ = Event::Quit;
                break;
        }
    }

    void Engine::Quit()
    {
        running_ = false;
    }
} // namespace tutorial
