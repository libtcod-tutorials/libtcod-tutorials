#ifndef EVENT_HPP
#define EVENT_HPP

namespace tutorial
{
    enum class Event : int
    {
        None = 0,
        MoveUp,
        MoveDown,
        MoveLeft,
        MoveRight,
        Quit
    };
}

#endif // EVENT_HPP
