#ifndef POSITION_HPP
#define POSITION_HPP

namespace tutorial
{
    struct Position
    {
        int x;
        int y;
    };
} // namespace tutorial

#endif // POSITION_HPP
