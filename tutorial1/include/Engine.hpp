#ifndef ENGINE_HPP
#define ENGINE_HPP

#include "Position.hpp"

#include <SDL2/SDL_events.h>
#include <libtcod/console_types.hpp>
#include <libtcod/context.hpp>

namespace tutorial
{
    enum class Event : int;

    class Engine
    {
    public:
        Engine();

        void Input();
        void Render();
        void Update();

        bool IsRunning() const;

    private:
        void HandleKeydownEvent(const SDL_Event& event);
        void Quit();

        tcod::Console console_;
        tcod::Context context_;

        Position player_pos_;
        Event next_event_;
        bool running_;
    };
} // namespace tutorial

#endif // ENGINE_HPP
