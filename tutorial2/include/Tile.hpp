#ifndef TILE_HPP
#define TILE_HPP

namespace tutorial
{
    enum class Tile : int
    {
        Floor,
        Wall,
    };
} // namespace tutorial

#endif // TILE_HPP
