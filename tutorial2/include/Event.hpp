#ifndef EVENT_HPP
#define EVENT_HPP

namespace tutorial
{
    enum class Event : int
    {
        None,
        MoveUp,
        MoveDown,
        MoveLeft,
        MoveRight,
        Quit
    };
}

#endif // EVENT_HPP
