#ifndef MAP_HPP
#define MAP_HPP

#include "Rectangle.hpp"

#include <libtcod/console_types.hpp>

#include <vector>

namespace tutorial
{
    enum class Tile : int;
    struct Position;

    class Map
    {
    public:
        explicit Map(Rectangle size);

        bool CanWalk(Position pos) const;
        void Render(tcod::Console& parent) const;

    private:
        void Render();
        void SetWall(Position pos);

        std::vector<Tile> tiles_;
        tcod::Console console_;

        Rectangle size_;
    };
} // namespace tutorial

#endif // MAP_HPP
