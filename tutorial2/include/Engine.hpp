#ifndef ENGINE_HPP
#define ENGINE_HPP

#include "Entity.hpp"
#include "Map.hpp"

#include <SDL2/SDL_events.h>
#include <libtcod/console_types.hpp>
#include <libtcod/context.hpp>

#include <vector>

namespace tutorial
{
    enum class Event : int;
    struct Configuration;
    struct Position;

    class Engine
    {
    public:
        Engine(const Configuration& config);

        void Input();
        void Render();
        void Update();

        bool IsRunning() const;

    private:
        void HandleKeydownEvent(const SDL_Event& event);
        void Quit();
        void RenderEntities();
        void RenderMap();

        bool CanMove(Position pos) const;

        Map map_;

        std::vector<Entity> entities_;

        tcod::Console console_;
        tcod::Context context_;

        Entity* player_;

        Event next_event_;

        bool running_;
    };
} // namespace tutorial

#endif // ENGINE_HPP
