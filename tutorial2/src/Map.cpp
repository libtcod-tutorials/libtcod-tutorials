#include "Map.hpp"

#include "Colors.hpp"
#include "Position.hpp"
#include "Rectangle.hpp"
#include "Tile.hpp"

#include <libtcod/color.h>
#include <libtcod/console.hpp>

#include <cstddef>

namespace tutorial
{
    constexpr Position indexToPos(std::size_t i, int width)
    {
        const int y = i / width;
        const int x = i - y * width;

        return Position { x, y };
    }

    constexpr std::size_t posToIndex(Position pos, int width)
    {
        return (pos.x + pos.y * width);
    }

    Map::Map(Rectangle size)
        : tiles_(size.width * size.height, Tile { Tile::Floor }),
          console_(size.width, size.height),
          size_(size)
    {
        this->SetWall({ 32, 32 });
        this->SetWall({ 33, 32 });

        this->Render();
    }

    bool Map::CanWalk(Position pos) const
    {
        const auto index = posToIndex(pos, size_.width);

        return tiles_.at(index) == Tile::Floor;
    }

    void Map::Render(tcod::Console& parent) const
    {
        tcod::blit(parent, console_);
    }

    void Map::Render()
    {
        console_.clear();

        for (std::size_t i = 0; i < tiles_.size(); ++i)
        {
            const auto pos = indexToPos(i, size_.width);

            TCOD_ColorRGB color { 0, 0, 0 };

            switch (tiles_.at(i))
            {
                case Tile::Floor:
                    color = color::LightAzure;
                    break;
                case Tile::Wall:
                    color = color::DarkAzure;
                    break;
            }

            console_.at({ pos.x, pos.y }).bg = color;
        }
    }

    void Map::SetWall(Position pos)
    {
        const auto index = posToIndex(pos, size_.width);

        tiles_.at(index) = Tile::Wall;
    }
} // namespace tutorial
