#include "Engine.hpp"

#include "Colors.hpp"
#include "Configuration.hpp"
#include "Event.hpp"
#include "Position.hpp"
#include "Rectangle.hpp"

#include <SDL2/SDL_events.h>
#include <SDL2/SDL_keycode.h>
#include <SDL2/SDL_video.h>
#include <libtcod/context.h>
#include <libtcod/version.h>
#include <libtcod/context.hpp>

#include <algorithm>

namespace tutorial
{
    Engine::Engine(const Configuration& config)
        : map_(config.window_size),
          console_(config.window_size.width, config.window_size.height),
          running_(true)
    {
        TCOD_ContextParams params {};
        params.tcod_version = TCOD_COMPILEDVERSION;
        params.console = console_.get();
        params.window_title = config.window_title.data();
        params.sdl_window_flags = SDL_WINDOW_RESIZABLE;

        context_ = tcod::Context { params };

        entities_.emplace_back(Position { config.window_size.width / 2,
                                          config.window_size.height / 2 },
                               color::White);

        entities_.emplace_back(Position { 63, 10 }, color::Yellow);

        player_ = &(entities_[0]);
    }

    void Engine::Input()
    {
        SDL_Event event {};

        SDL_WaitEvent(&event);

        switch (event.type)
        {
            case SDL_QUIT:
                next_event_ = Event::Quit;
                break;
            case SDL_KEYDOWN:
                HandleKeydownEvent(event);
                break;
            default:
                next_event_ = Event::None;
                break;
        }

        SDL_FlushEvent(SDL_KEYDOWN);
    }

    void Engine::Render()
    {
        console_.clear();

        RenderMap();
        RenderEntities();

        context_.present(console_);
    }

    void Engine::Update()
    {
        auto new_pos = player_->GetPos();

        switch (next_event_)
        {
            case Event::MoveUp:
                --new_pos.y;
                break;
            case Event::MoveDown:
                ++new_pos.y;
                break;
            case Event::MoveLeft:
                --new_pos.x;
                break;
            case Event::MoveRight:
                ++new_pos.x;
                break;
            case Event::Quit:
                Quit();
                break;
            default:
                break;
        }

        if (CanMove(new_pos))
        {
            player_->SetPos(new_pos);
        }
    }

    bool Engine::IsRunning() const
    {
        return running_;
    }

    void Engine::HandleKeydownEvent(const SDL_Event& event)
    {
        switch (event.key.keysym.sym)
        {
            case SDLK_UP:
                next_event_ = Event::MoveUp;
                break;
            case SDLK_DOWN:
                next_event_ = Event::MoveDown;
                break;
            case SDLK_LEFT:
                next_event_ = Event::MoveLeft;
                break;
            case SDLK_RIGHT:
                next_event_ = Event::MoveRight;
                break;
            case SDLK_ESCAPE:
                next_event_ = Event::Quit;
                break;
        }
    }

    void Engine::Quit()
    {
        running_ = false;
    }

    void Engine::RenderEntities()
    {
        // Show the position of each entity
        std::for_each(entities_.cbegin(), entities_.cend(),
                      [&](const auto entity)
                      {
                          constexpr char icon = '@';

                          const auto pos = entity.GetPos();
                          console_.at({ pos.x, pos.y }).ch = icon;

                          const auto color = entity.GetColor();
                          console_.at({ pos.x, pos.y }).fg = color;
                      });
    }

    void Engine::RenderMap()
    {
        map_.Render(console_);
    }

    bool Engine::CanMove(Position pos) const
    {
        if (!console_.in_bounds({ pos.x, pos.y }))
        {
            return false;
        }

        if (!map_.CanWalk(pos))
        {
            return false;
        }

        for (auto entity : entities_)
        {
            if (pos == entity.GetPos())
            {
                return false;
            }
        }

        return true;
    }
} // namespace tutorial
