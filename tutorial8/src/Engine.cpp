#include "Engine.hpp"

#include "Colors.hpp"
#include "Entity.hpp"
#include "EntityFactory.hpp"
#include "Event.hpp"
#include "EventHandler.hpp"
#include "HealthBar.hpp"
#include "Map.hpp"
#include "MapGenerator.hpp"
#include "MessageHistoryWindow.hpp"
#include "MessageLogWindow.hpp"

#include <libtcod/console.hpp>
#include <libtcod/sys.hpp>

#include <memory>
#include <string>

namespace tutorial
{
    inline namespace
    {
        constexpr int kUiHeight = 5;

        constexpr int kRoomMinSize = 6;
        constexpr int kRoomMaxSize = 10;
        constexpr int kMaxRooms = 30;

        constexpr int kFovRadius = 10;

        constexpr int kMaxMonstersPerRoom = 3;
    } // namespace

    // Public methods
    Engine::Engine(const Configuration& config)
        : config_(config),
          eventHandler_(std::make_unique<MainGameEventHandler>(*this)),
          map_(std::make_unique<Map>(config.width, config.height - kUiHeight)),
          messageHistoryWindow_(std::make_unique<MessageHistoryWindow>(
              config.width, config.height, pos_t { 0, 0 }, messageLog_)),
          messageLogWindow_(std::make_unique<MessageLogWindow>(
              40, 5, pos_t { 21, 45 }, messageLog_)),
          player_(nullptr),
          healthBar_(nullptr),
          window_(MainGame),
          gameOver_(false),
          running_(true)
    {
        // Initialize the tcod root console
        TCODConsole::initRoot(config.width, config.height,
                              config.title.c_str());
        TCODSystem::setFps(config.fps);

        this->NewGame();
    }

    Engine::~Engine()
    {
        // Need to call this before we exit to clean tcod up
        TCOD_quit();
    }

    void Engine::AddEventFront(Event_ptr& event)
    {
        eventQueue_.push_front(std::move(event));
    }

    void Engine::ComputeFOV()
    {
        map_->ComputeFov(player_->GetPos(), kFovRadius);
        map_->Update();
    }

    void Engine::GetInput()
    {
        auto action = eventHandler_->Dispatch();

        if (action.get() != nullptr)
        {
            this->AddEvent(action);

            if (!gameOver_)
            {
                this->HandleEnemyTurns();
            }
        }
    }

    void Engine::HandleDeathEvent(Entity& entity)
    {
        if (this->IsPlayer(entity))
        {
            eventHandler_ = std::make_unique<GameOverEventHandler>(*this);
            eventQueue_.clear();
            gameOver_ = true;
        }

        entity.Die();

        // Move entity to front of entity list so that the corpse is
        // rendered first This prevents live entities from being rendered
        // underneath a corpse
        entities_.MoveToFront(entity);
    }

    void Engine::HandleEvents()
    {
        while (!eventQueue_.empty())
        {
            auto event = std::move(eventQueue_.front());
            eventQueue_.pop_front();
            event->Execute();
        }

        eventQueue_.clear();
    }

    void Engine::LogMessage(const std::string& text, TCODColor color,
                            bool stack)
    {
        messageLog_.AddMessage(text, color, stack);
    }

    void Engine::NewGame()
    {
        // Clear the entities and message log
        entities_.Clear();
        messageLog_.Clear();
        eventQueue_.clear();

        this->GenerateMap(config_.width, config_.height - kUiHeight);

        // Place monsters
        auto rooms = map_->GetRooms();

        for (auto it = rooms.begin() + 1; it != rooms.end(); ++it)
        {
            entities_.PlaceEntities(*it, kMaxMonstersPerRoom);
        }

        // Create player and add them to entity list
        PlayerFactory factory {};
        player_ = entities_.Spawn(factory.Create(), rooms[0].GetCenter()).get();

        // Create health bar
        healthBar_ =
            std::make_unique<HealthBar>(20, 1, pos_t { 0, 45 }, *player_);

        this->ComputeFOV();

        messageLog_.AddMessage("Hello and welcome to the C++ libtcod dungeon!",
                               color::light_azure, false);

        window_ = MainGame;

        eventHandler_ = std::make_unique<MainGameEventHandler>(*this);

        gameOver_ = false;
    }

    void Engine::ReturnToMainGame()
    {
        if (window_ != MainGame)
        {
            eventHandler_ = std::make_unique<MainGameEventHandler>(*this);
            window_ = MainGame;
        }
    }

    void Engine::ShowMessageHistory()
    {
        if (window_ != MessageHistory)
        {
            eventHandler_ = std::make_unique<MessageHistoryEventHandler>(*this);
            window_ = MessageHistory;
        }
    }

    void Engine::Quit()
    {
        std::exit(0);
    }

    Entity* Engine::GetBlockingEntity(pos_t pos) const
    {
        return entities_.GetBlockingEntity(pos);
    }

    Entity* Engine::GetPlayer() const
    {
        return player_;
    }

    bool Engine::IsBlocker(pos_t pos) const
    {
        if (this->GetBlockingEntity(pos))
        {
            return true;
        }

        if (this->IsWall(pos))
        {
            return true;
        }

        return false;
    }

    bool Engine::IsInBounds(pos_t pos) const
    {
        return map_->IsInBounds(pos);
    }

    bool Engine::IsInFov(pos_t pos) const
    {
        return map_->IsInFov(pos);
    }

    bool Engine::IsPlayer(const Entity& entity) const
    {
        return player_ == &entity;
    }

    bool Engine::IsRunning() const
    {
        return (!TCODConsole::isWindowClosed() && running_);
    }

    bool Engine::IsValid(Entity& entity) const
    {
        auto it = std::find_if(entities_.begin(), entities_.end(),
                               [&entity](const auto& it)
                               { return (it.get() == &entity); });

        return (it != entities_.end());
    }

    bool Engine::IsWall(pos_t pos) const
    {
        return map_->IsWall(pos);
    }

    void Engine::Render() const
    {
        auto* root = TCODConsole::root;

        // Clear the console
        root->clear();

        if (window_ == MainGame)
        {
            // Render the map
            map_->Render(root);

            // Show all entitys positions if in fov
            for (const auto& entity : entities_)
            {
                const auto pos = entity->GetPos();

                if (map_->IsInFov(pos))
                {
                    const auto& renderable = entity->GetRenderable();

                    renderable->Render(root, pos);
                }
            }

            // Render UI elements on top
            healthBar_->Render(root);
            messageLogWindow_->Render(root);
        }
        else if (window_ == MessageHistory)
        {
            messageHistoryWindow_->Render(root);
        }

        // Flushing the console redraws it to the screen
        root->flush();
    }

    // Private methods
    void Engine::AddEvent(Event_ptr& event)
    {
        eventQueue_.push_back(std::move(event));
    }

    void Engine::GenerateMap(int width, int height)
    {
        Map::Generator generator(
            { kMaxRooms, kRoomMinSize, kRoomMaxSize, width, height });

        map_->Generate(generator);
        map_->Update();
    }

    void Engine::HandleEnemyTurns()
    {
        for (auto& entity : entities_)
        {
            if (IsPlayer(*entity.get()))
            {
                continue;
            }

            if (!entity->CanAct())
            {
                continue;
            }

            std::unique_ptr<Event> event =
                std::make_unique<AiAction>(*this, *entity);
            this->AddEvent(event);
        }
    }
} // namespace tutorial
