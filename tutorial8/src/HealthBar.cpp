#include "HealthBar.hpp"

#include "Colors.hpp"
#include "Components.hpp"

namespace tutorial
{
    HealthBar::HealthBar(unsigned int width, unsigned int height, pos_t pos,
                         const Entity& entity)
        : UiWindowBase(width, height, pos), entity_(entity)
    {
        console_->setDefaultBackground(color::dark_red);
        console_->rect(0, 0, console_->getWidth(), console_->getHeight(), true);
    }

    void HealthBar::Render(TCODConsole* parent) const
    {
        console_->clear();

        auto health = entity_.GetDestructible();

        if (health != nullptr)
        {
            const auto width =
                (int)((float)(health->GetHealth()) / health->GetMaxHealth()
                      * console_->getWidth());

            if (width > 0)
            {
                for (int i = 0; i < width; ++i)
                {
                    console_->setCharBackground(i, console_->getHeight() - 1,
                                                color::green);
                }
            }

            console_->printf(1, 0, "HP: %i/%i", health->GetHealth(),
                             health->GetMaxHealth());
        }

        TCODConsole::blit(console_.get(), 0, 0, console_->getWidth(),
                          console_->getHeight(), parent, pos_.x, pos_.y);
    }
} // namespace tutorial
