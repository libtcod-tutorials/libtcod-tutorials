#include "EventHandler.hpp"

#include "Engine.hpp"
#include "Event.hpp"

#include "libtcod/sys.hpp"

#include <array>
#include <functional>
#include <memory>
#include <stdexcept>
#include <unordered_map>

namespace tutorial
{
    inline namespace
    {
        constexpr std::size_t kNumActions = 9;

        static const std::array<std::function<std::unique_ptr<Event>(Engine&)>,
                                kNumActions>
            kGameActions {
                // Up
                [](auto& engine)
                {
                    return std::make_unique<BumpAction>(
                        engine, *engine.GetPlayer(), pos_t { 0, -1 });
                },
                // Down
                [](auto& engine)
                {
                    return std::make_unique<BumpAction>(
                        engine, *engine.GetPlayer(), pos_t { 0, 1 });
                },
                // Left
                [](auto& engine)
                {
                    return std::make_unique<BumpAction>(
                        engine, *engine.GetPlayer(), pos_t { -1, 0 });
                },
                // Right
                [](auto& engine)
                {
                    return std::make_unique<BumpAction>(
                        engine, *engine.GetPlayer(), pos_t { 1, 0 });
                },
                // Wait action
                [](auto& engine) {
                    return std::make_unique<WaitAction>(engine,
                                                        *engine.GetPlayer());
                },
                // Message history event
                [](auto& engine)
                { return std::make_unique<MessageHistoryEvent>(engine); },
                // Return to game event
                [](auto& engine)
                { return std::make_unique<ReturnToGameEvent>(engine); },
                // New game
                [](auto& engine)
                { return std::make_unique<NewGameEvent>(engine); },
                // Exit
                [](auto& engine) { return std::make_unique<QuitEvent>(engine); }
            };
    } // namespace

    BaseEventHandler::BaseEventHandler(Engine& engine) : engine_(engine)
    {
    }

    void BaseEventHandler::SetKeyMap(
        const std::unordered_map<KeyPress, Actions, KeyPressHash>& keyMap)
    {
        keyMap_ = keyMap;
    }

    std::unique_ptr<Event> BaseEventHandler::Dispatch() const
    {
        // Check for event sees if anything is in the input event queue and
        // If no event, return
        TCOD_key_t key {};

        TCODSystem::checkForEvent(TCOD_EVENT_KEY_PRESS, &key, nullptr);

        std::unique_ptr<Event> event { nullptr };

        try
        {
            KeyPress keypress { key.vk, key.vk == TCODK_CHAR ? key.c : '\0' };

            auto action = keyMap_.at(keypress);
            event = kGameActions.at(action)(engine_);
        }
        catch (std::out_of_range&)
        {
            // no op
        }

        return event;
    }

    static const std::unordered_map<KeyPress, Actions, KeyPressHash>
        MainGameKeyMap { { TCODK_UP, Actions::MOVE_UP },
                         { TCODK_DOWN, Actions::MOVE_DOWN },
                         { TCODK_LEFT, Actions::MOVE_LEFT },
                         { TCODK_RIGHT, Actions::MOVE_RIGHT },
                         { TCODK_SPACE, Actions::WAIT },
                         { { TCODK_CHAR, 'v' }, Actions::MESSAGE_HISTORY },
                         { TCODK_ENTER, Actions::NEW_GAME },
                         { TCODK_ESCAPE, Actions::QUIT } };

    MainGameEventHandler::MainGameEventHandler(Engine& engine)
        : BaseEventHandler(engine)
    {
        SetKeyMap(MainGameKeyMap);
    }

    static const std::unordered_map<KeyPress, Actions, KeyPressHash>
        MessageHistoryKeyMap { { { TCODK_CHAR, 'v' },
                                 Actions::RETURN_TO_GAME } };

    MessageHistoryEventHandler::MessageHistoryEventHandler(Engine& engine)
        : BaseEventHandler(engine)
    {
        SetKeyMap(MessageHistoryKeyMap);
    }

    static const std::unordered_map<KeyPress, Actions, KeyPressHash>
        GameOverKeyMap { { TCODK_ENTER, Actions::NEW_GAME },
                         { TCODK_ESCAPE, Actions::QUIT } };

    GameOverEventHandler::GameOverEventHandler(Engine& engine)
        : BaseEventHandler(engine)
    {
        SetKeyMap(GameOverKeyMap);
    }
} // namespace tutorial
