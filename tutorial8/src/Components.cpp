#include "Components.hpp"

namespace tutorial
{
    inline namespace
    {
        using uint = unsigned int;
    }

    AttackerComponent::AttackerComponent(uint power) : power_(power)
    {
    }

    uint AttackerComponent::Attack() const
    {
        return power_;
    }

    DestructibleComponent::DestructibleComponent(uint defense, uint hp)
        : defense_(defense), maxHp_(hp), hp_(hp)
    {
    }

    DestructibleComponent::DestructibleComponent(uint defense, uint maxHp,
                                                 uint hp)
        : defense_(defense), maxHp_(maxHp), hp_(hp)
    {
    }

    void DestructibleComponent::Heal(uint value)
    {
        hp_ += std::min<uint>(maxHp_, hp_ + value);
    }

    void DestructibleComponent::TakeDamage(uint value)
    {
        hp_ -= std::max<uint>(0, std::min<uint>(value, maxHp_));
    }

    uint DestructibleComponent::GetDefense() const
    {
        return defense_;
    }

    int DestructibleComponent::GetHealth() const
    {
        return hp_;
    }

    uint DestructibleComponent::GetMaxHealth() const
    {
        return maxHp_;
    }

    bool DestructibleComponent::IsDead() const
    {
        return (hp_ <= 0);
    }

    IconRenderable::IconRenderable(TCODColor color, char icon)
        : color_(color), icon_(icon)
    {
    }

    void IconRenderable::Render(TCODConsole* parent, pos_t pos) const
    {
        parent->setChar(pos.x, pos.y, icon_);
        parent->setCharForeground(pos.x, pos.y, color_);
    }
} // namespace tutorial
