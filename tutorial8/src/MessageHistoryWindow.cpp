#include "MessageHistoryWindow.hpp"

#include "Colors.hpp"

namespace tutorial
{
    MessageHistoryWindow::MessageHistoryWindow(std::size_t width,
                                               std::size_t height, pos_t pos,
                                               const MessageLog& log)
        : UiWindowBase(width, height, pos), log_(log)
    {
        console_->setDefaultBackground(color::black);
    }

    void MessageHistoryWindow::Render(TCODConsole* parent) const
    {
        auto messages = log_.GetMessages();

        console_->clear();

        int y_offset = console_->getHeight() - 1;

        for (auto it = messages.rbegin(); it != messages.rend(); ++it)
        {
            auto line_height = console_->getHeightRect(
                0, 0, console_->getWidth(), console_->getHeight(), "%s",
                it->text.c_str());

            if (line_height > 1)
            {
                y_offset -= line_height - 1;
            }

            {
                auto line = std::make_unique<TCODConsole>(console_->getWidth(),
                                                          line_height);

                line->setDefaultForeground(it->color);
                line->printRect(0, 0, line->getWidth(), line->getHeight(), "%s",
                                it->text.c_str());

                TCODConsole::blit(line.get(), 0, 0, line->getWidth(),
                                  line->getHeight(), console_.get(), 0,
                                  y_offset);
            }

            --y_offset;

            if (y_offset < 0)
            {
                break;
            }
        }

        TCODConsole::blit(console_.get(), 0, 0, console_->getWidth(),
                          console_->getHeight(), parent, pos_.x, pos_.y);
    }
} // namespace tutorial
