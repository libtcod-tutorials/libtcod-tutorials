#include "Map.hpp"

#include "Colors.hpp"
#include "Entity.hpp"
#include "EntityManager.hpp"
#include "MapGenerator.hpp"
#include "Util.hpp"

#include <memory>

namespace tutorial
{
    Map::Map(int width, int height)
        : tiles_(std::vector<tile_t>(width * height,
                                     tile_t { TileType::WALL, false })),
          console_(std::make_unique<TCODConsole>(width, height)),
          map_(std::make_unique<TCODMap>(width, height)),
          width_(width),
          height_(height)
    {
    }

    void Map::ComputeFov(pos_t origin, int fovRadius)
    {
        map_->computeFov(origin.x, origin.y, fovRadius);
    }

    void Map::Generate(Generator& generator)
    {
        Clear();

        generator.Generate(*this);
    }

    void Map::SetExplored(pos_t pos, bool explored)
    {
        tiles_.at(util::posToIndex(pos, width_)).explored = explored;
    }

    void Map::SetTileType(pos_t pos, TileType type)
    {
        tiles_.at(util::posToIndex(pos, width_)).type = type;

        switch (type)
        {
            case TileType::FLOOR:
                map_->setProperties(pos.x, pos.y, true, true);
                break;
            case TileType::WALL:
                map_->setProperties(pos.x, pos.y, false, false);
                break;
            default:
                break;
        }
    }

    void Map::Update()
    {
        console_->clear();

        for (std::size_t i = 0; i < tiles_.size(); ++i)
        {
            const auto pos = util::indexToPos(i, width_);

            TCODColor color {};
            auto& tile = tiles_.at(i);

            if (IsInFov(pos))
            {
                tile.explored = true;

                switch (tile.type)
                {
                    case TileType::FLOOR:
                        color = color::light_amber;
                        break;
                    case TileType::WALL:
                        color = color::dark_amber;
                        break;
                    default:
                        break;
                }
            }
            else if (IsExplored(pos))
            {
                switch (tile.type)
                {
                    case TileType::FLOOR:
                        color = color::light_azure;
                        break;
                    case TileType::WALL:
                        color = color::dark_azure;
                        break;
                    default:
                        break;
                }
            }

            console_->setCharBackground(pos.x, pos.y, color);
        }
    }

    int Map::GetHeight() const
    {
        return height_;
    }

    const std::vector<Room>& Map::GetRooms() const
    {
        return rooms_;
    }

    TileType Map::GetTileType(pos_t pos) const
    {
        return tiles_.at(util::posToIndex(pos, width_)).type;
    }

    int Map::GetWidth() const
    {
        return width_;
    }

    bool Map::IsInBounds(pos_t pos) const
    {
        return (pos.x >= 0 && pos.y >= 0 && pos.x < width_ && pos.y < height_);
    }

    bool Map::IsExplored(pos_t pos) const
    {
        return tiles_.at(util::posToIndex(pos, width_)).explored;
    }

    bool Map::IsInFov(pos_t pos) const
    {
        return map_->isInFov(pos.x, pos.y);
    }

    bool Map::IsWall(pos_t pos) const
    {
        return !map_->isWalkable(pos.x, pos.y);
    }

    void Map::Render(TCODConsole* parent) const
    {
        TCODConsole::blit(console_.get(), 0, 0, width_, height_, parent, 0, 0);
    }

    void Map::Clear()
    {
        rooms_.clear();
        map_->clear();

        for (auto& tile : tiles_)
        {
            tile.explored = false;
            tile.type = TileType::WALL;
        }
    }
} // namespace tutorial
