#ifndef COMPONENTS_HPP
#define COMPONENTS_HPP

#include "Position.hpp"

#include <libtcod/color.hpp>
#include <libtcod/console.hpp>

namespace tutorial
{
    class AttackerComponent
    {
    public:
        AttackerComponent(unsigned int power);

        unsigned int Attack() const;

    private:
        unsigned int power_;
    };

    class DestructibleComponent
    {
    public:
        DestructibleComponent(unsigned int defense, unsigned int hp);
        DestructibleComponent(unsigned int defense, unsigned int maxHp,
                              unsigned int hp);

        void Heal(unsigned int value);
        void TakeDamage(unsigned int value);

        unsigned int GetDefense() const;
        int GetHealth() const;
        unsigned int GetMaxHealth() const;
        bool IsDead() const;

    private:
        unsigned int defense_;
        unsigned int maxHp_;
        int hp_;
    };

    class RenderableComponent
    {
    public:
        virtual ~RenderableComponent() = default;

        virtual void Render(TCODConsole* parent, pos_t pos) const = 0;
    };

    class IconRenderable : public RenderableComponent
    {
    public:
        IconRenderable(TCODColor color, char icon);

        void Render(TCODConsole* parent, pos_t pos) const override;

    private:
        TCODColor color_;
        char icon_;
    };
} // namespace tutorial

#endif // COMPONENTS_HPP
