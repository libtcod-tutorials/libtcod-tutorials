#ifndef EVENT_HANDLER_HPP
#define EVENT_HANDLER_HPP

#include "AnEventHandler.hpp"

#include <memory>
#include <vector>

namespace tutorial
{
    struct Position;
    struct Rectangle;

    class AnEvent;
    class Engine;
    class GenerateMapEvent;
    class MoveUpEvent;
    class MoveDownEvent;
    class MoveLeftEvent;
    class MoveRightEvent;
    class QuitEvent;

    class EventHandler
    {
    public:
        template <typename T>
        void AddHandler(T&& handler)
        {
            handlers_.push_back(std::make_unique<T>(handler));
        }

        void Handle(AnEvent& event);

    private:
        using EventHandler_UPtr = std::unique_ptr<DefaultEventHandler>;
        std::vector<EventHandler_UPtr> handlers_ {};
    };

    class GenerateMapEventHandler final : public DefaultEventHandler
    {
    public:
        GenerateMapEventHandler(Engine& engine);

        using DefaultEventHandler::Handle;
        void Handle(GenerateMapEvent& event) override;

    private:
        void GenerateMap(Rectangle size);

        Engine& engine_;
    };

    class QuitEventHandler final : public DefaultEventHandler
    {
    public:
        QuitEventHandler(Engine& engine);

        using DefaultEventHandler::Handle;
        void Handle(QuitEvent& event) override;

    private:
        Engine& engine_;
    };

    class MoveEventHandler final : public DefaultEventHandler
    {
    public:
        MoveEventHandler(Engine& engine);

        using DefaultEventHandler::Handle;
        void Handle(MoveUpEvent& event) override;
        void Handle(MoveDownEvent& event) override;
        void Handle(MoveLeftEvent& event) override;
        void Handle(MoveRightEvent& event) override;

    private:
        bool CanWalk(Position pos) const;

        Engine& engine_;
    };
} // namespace tutorial

#endif // EVENT_HANDLER_HPP
