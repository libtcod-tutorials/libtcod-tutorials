#ifndef ENTITY_HPP
#define ENTITY_HPP

#include "Position.hpp"
#include "Renderable.hpp"

namespace tcod
{
    class Console;
}

namespace tutorial
{
    class Entity
    {
    public:
        Entity(Position pos, Renderable renderable);

        void SetPos(Position pos);

        Position GetPos() const;
        void Render(tcod::Console& console) const;

    private:
        Position pos_;
        Renderable renderable_;
    };
} // namespace tutorial

#endif // ENTITY_HPP
