#ifndef ENGINE_HPP
#define ENGINE_HPP

#include "Entity.hpp"
#include "EventHandler.hpp"
#include "Map.hpp"
#include "Renderer.hpp"

#include <memory>
#include <vector>

namespace tutorial
{
    struct Configuration;
    struct Rectangle;

    class Engine
    {
    public:
        Engine(const Configuration& config);

        void ComputeFOV();
        std::vector<Entity>& GetEntities();
        void HandleEvents();
        void Render();
        void SetMap(Map&& map);
        void SetPlayer(Entity* entity);
        void Quit();

        Map* GetMap() const;
        Entity* GetPlayer() const;
        bool IsRunning() const;

    private:
        std::vector<Entity> entities_;

        EventHandler event_handler_;
        Renderer renderer_;

        std::unique_ptr<Map> map_;

        Entity* player_;

        bool running_;
    };
} // namespace tutorial

#endif // ENGINE_HPP
