#ifndef ROOM_HPP
#define ROOM_HPP

#include "Position.hpp"

#include <vector>

namespace tutorial
{
    class Room
    {
    public:
        Room(Position origin, int width, int height);

        Position GetCenter() const;
        Position GetOrigin() const;
        std::vector<Position> GetInner() const;
        bool Intersects(Room& other) const;

    private:
        Position origin_; // Top-left corner
        Position end_;    // Bottom-right corner
    };
} // namespace tutorial

#endif // ROOM_HPP
