#ifndef AN_EVENT_HPP
#define AN_EVENT_HPP

#include "AnEventHandler.hpp"

namespace tutorial
{
    class AnEvent
    {
    public:
        virtual ~AnEvent() = default;

        virtual void Handle(AnEventHandler&) = 0;
    };

    template <typename T>
    class HandleableEvent : public AnEvent
    {
    public:
        void Handle(AnEventHandler& handler) override
        {
            handler.Handle(*static_cast<T*>(this));
        }
    };
} // namespace tutorial

#endif // AN_EVENT_HPP
