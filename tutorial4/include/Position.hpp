#ifndef POSITION_HPP
#define POSITION_HPP

namespace tutorial
{
    struct Position
    {
        int x;
        int y;
    };

    constexpr bool operator==(Position lhs, Position rhs)
    {
        return (lhs.x == rhs.x && lhs.y == rhs.y);
    }

    constexpr bool operator!=(Position lhs, Position rhs)
    {
        return !(lhs == rhs);
    }

    constexpr Position operator+(Position lhs, Position rhs)
    {
        return Position { lhs.x + rhs.x, lhs.y + rhs.y };
    }

    constexpr Position operator/(Position lhs, int rhs)
    {
        return Position { lhs.x / rhs, lhs.y / rhs };
    }
} // namespace tutorial

#endif // POSITION_HPP
