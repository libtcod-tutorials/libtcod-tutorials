#ifndef RECTANGLE_HPP
#define RECTANGLE_HPP

namespace tutorial
{
    struct Rectangle
    {
        int width;
        int height;
    };
} // namespace tutorial

#endif // RECTANGLE_HPP
