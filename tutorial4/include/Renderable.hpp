#ifndef RENDERABLE_HPP
#define RENDERABLE_HPP

#include <libtcod/color.hpp>

namespace tcod
{
    class Console;
}

namespace tutorial
{
    struct Position;

    class Renderable
    {
    public:
        Renderable(tcod::ColorRGB color, char icon);

        void Render(tcod::Console& console, Position pos) const;

    private:
        tcod::ColorRGB color_;
        char icon_;
    };
} // namespace tutorial

#endif // RENDERABLE_HPP
