#ifndef CONFIGURATION_HPP
#define CONFIGURATION_HPP

#include "Rectangle.hpp"

#include <string_view>

namespace tutorial
{
    struct Configuration
    {
        std::string_view window_title;
        Rectangle window_size;
    };
} // namespace tutorial

#endif // CONFIGURATION_HPP
