#ifndef EVENT_HPP
#define EVENT_HPP

#include "AnEvent.hpp"

namespace tutorial
{
    class Entity;

    class GenerateMapEvent final : public HandleableEvent<GenerateMapEvent>
    {
    };

    class QuitEvent final : public HandleableEvent<QuitEvent>
    {
    };

    class MoveUpEvent final : public HandleableEvent<MoveUpEvent>
    {
    };

    class MoveDownEvent final : public HandleableEvent<MoveDownEvent>
    {
    };

    class MoveLeftEvent final : public HandleableEvent<MoveLeftEvent>
    {
    };

    class MoveRightEvent final : public HandleableEvent<MoveRightEvent>
    {
    };
} // namespace tutorial

#endif // EVENT_HPP
