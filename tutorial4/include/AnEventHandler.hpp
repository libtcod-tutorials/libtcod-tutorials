#ifndef AN_EVENT_HANDLER_HPP
#define AN_EVENT_HANDLER_HPP

namespace tutorial
{
    class AnEvent;
    class GenerateMapEvent;
    class MoveUpEvent;
    class MoveDownEvent;
    class MoveLeftEvent;
    class MoveRightEvent;
    class QuitEvent;

    class AnEventHandler
    {
    public:
        virtual ~AnEventHandler() = default;

        virtual void Handle(AnEvent&) = 0;
        virtual void Handle(GenerateMapEvent&) = 0;
        virtual void Handle(MoveUpEvent&) = 0;
        virtual void Handle(MoveDownEvent&) = 0;
        virtual void Handle(MoveLeftEvent&) = 0;
        virtual void Handle(MoveRightEvent&) = 0;
        virtual void Handle(QuitEvent&) = 0;
    };

    template <typename T>
    class SingleEventHandler : virtual public AnEventHandler
    {
    public:
        using AnEventHandler::Handle;
        void Handle(T&) override
        {
        }
    };

    template <typename... T>
    class MultiEventHandler : public SingleEventHandler<T>...
    {
    public:
        using SingleEventHandler<T>::Handle...;
    };

    class DefaultEventHandler
        : public MultiEventHandler<AnEvent, GenerateMapEvent, MoveUpEvent,
                                   MoveDownEvent, MoveLeftEvent, MoveRightEvent,
                                   QuitEvent>
    {
    };
} // namespace tutorial

#endif // AN_EVENT_HANDLER_HPP
