#include "Entity.hpp"
#include "Renderable.hpp"

#include <libtcod/console_types.hpp>

namespace tutorial
{
    Entity::Entity(Position pos, Renderable renderable)
        : pos_(pos), renderable_(renderable)
    {
    }

    void Entity::SetPos(Position pos)
    {
        pos_ = pos;
    }

    Position Entity::GetPos() const
    {
        return pos_;
    }

    void Entity::Render(tcod::Console& console) const
    {
        renderable_.Render(console, pos_);
    }
} // namespace tutorial
