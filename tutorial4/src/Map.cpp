#include "Map.hpp"

#include "Colors.hpp"
#include "Position.hpp"
#include "Rectangle.hpp"
#include "Tile.hpp"

#include <libtcod/color.h>
#include <libtcod/color.hpp>
#include <libtcod/console.hpp>

#include <exception>
#include <memory>
#include <stdexcept>
#include <utility>

namespace tutorial
{
    constexpr Position indexToPos(int i, int width)
    {
        const int y = i / width;
        const int x = i - y * width;

        return Position { x, y };
    }

    constexpr int posToIndex(Position pos, int width)
    {
        return (pos.x + pos.y * width);
    }

    Map::Map(Rectangle size)
        : tiles_(std::vector<tile_t>(size.width * size.height,
                                     tile_t { false, TileType::WALL })),
          map_(std::make_unique<TCODMap>(size.width, size.height)),
          console_(size.width, size.height),
          size_(size)
    {
    }

    void Map::ComputeFov(Position origin, int fovRadius)
    {
        map_->computeFov(origin.x, origin.y, fovRadius);
    }

    void Map::SetExplored(Position pos, bool explored)
    {
        const auto index = posToIndex(pos, size_.width);
        tiles_.at(index).explored = explored;
    }

    void Map::SetTileType(Position pos, TileType type)
    {
        const auto index = posToIndex(pos, size_.width);
        tiles_.at(index).type = type;

        switch (type)
        {
            case TileType::FLOOR:
                map_->setProperties(pos.x, pos.y, true, true);
                break;
            case TileType::WALL:
                map_->setProperties(pos.x, pos.y, false, false);
                break;
            default:
                break;
        }
    }

    void Map::Update()
    {
        console_.clear();

        for (std::size_t i = 0; i < tiles_.size(); ++i)
        {
            const auto pos = indexToPos(i, size_.width);
            auto& tile = tiles_.at(i);

            tcod::ColorRGB color {};

            if (this->IsInFov(pos))
            {
                tile.explored = true;

                switch (tile.type)
                {
                    case TileType::FLOOR:
                        color = color::LightAmber;
                        break;
                    case TileType::WALL:
                        color = color::DarkAmber;
                        break;
                    default:
                        break;
                }
            }
            else if (this->IsExplored(pos))
            {
                switch (tile.type)
                {
                    case TileType::FLOOR:
                        color = color::LightAzure;
                        break;
                    case TileType::WALL:
                        color = color::DarkAzure;
                        break;
                    default:
                        break;
                }
            }

            console_.at({ pos.x, pos.y }).bg = color;
        }
    }

    Room Map::GetRoom(int index) const
    {
        try
        {
            return rooms_.at(index);
        }
        catch (const std::exception&)
        {
            throw std::out_of_range("Room does not exist");
        }
    }

    Rectangle Map::GetSize() const
    {
        return size_;
    }

    TileType Map::GetTileType(Position pos) const
    {
        const auto index = posToIndex(pos, size_.width);
        return tiles_.at(index).type;
    }

    bool Map::IsInBounds(Position pos) const
    {
        return (pos.x >= 0 && pos.y >= 0 && pos.x < size_.width
                && pos.y < size_.height);
    }

    bool Map::IsExplored(Position pos) const
    {
        const auto index = posToIndex(pos, size_.width);
        return tiles_.at(index).explored;
    }

    bool Map::IsInFov(Position pos) const
    {
        return map_->isInFov(pos.x, pos.y);
    }

    bool Map::IsWall(Position pos) const
    {
        return !map_->isWalkable(pos.x, pos.y);
    }

    void Map::Render(tcod::Console& parent) const
    {
        tcod::blit(parent, console_);
    }
} // namespace tutorial
