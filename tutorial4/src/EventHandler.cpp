#include "EventHandler.hpp"

#include "AnEvent.hpp"
#include "AnEventHandler.hpp"
#include "Colors.hpp"
#include "Engine.hpp"
#include "EngineEvent.hpp"
#include "Entity.hpp"
#include "Map.hpp"
#include "MapGenerator.hpp"
#include "Position.hpp"
#include "Rectangle.hpp"
#include "Renderable.hpp"
#include "Room.hpp"

namespace tutorial
{
    void EventHandler::Handle(AnEvent& event)
    {
        for (const auto& handler : handlers_)
        {
            event.Handle(*handler);
        }
    }

    GenerateMapEventHandler::GenerateMapEventHandler(Engine& engine)
        : engine_(engine)
    {
    }

    void GenerateMapEventHandler::Handle(GenerateMapEvent&)
    {
        if (auto map = engine_.GetMap())
        {
            this->GenerateMap(map->GetSize());
        }
    }

    void GenerateMapEventHandler::GenerateMap(Rectangle size)
    {
        auto& entities = engine_.GetEntities();
        entities.clear();

        constexpr int max_rooms = 30;
        constexpr int min_room_size = 6;
        constexpr int max_room_size = 10;
        const GeneratorConfiguration config { size, max_rooms, min_room_size,
                                              max_room_size };

        auto map = Map::Generator::Generate(config);
        map.Update();

        // Create player and add them to entity list
        constexpr char player_icon = '@';

        auto room = map.GetRoom(0);
        entities.emplace_back(room.GetCenter(),
                              Renderable { color::White, player_icon });

        // Get pointer to player
        auto player = &entities.at(0);

        // Set player and map
        engine_.SetMap(std::move(map));
        engine_.SetPlayer(player);
        engine_.ComputeFOV();
    }

    QuitEventHandler::QuitEventHandler(Engine& engine) : engine_(engine)
    {
    }

    void QuitEventHandler::Handle(QuitEvent&)
    {
        engine_.Quit();
    }

    MoveEventHandler::MoveEventHandler(Engine& engine) : engine_(engine)
    {
    }

    void MoveEventHandler::Handle(MoveUpEvent&)
    {
        auto* player = engine_.GetPlayer();
        auto new_pos = player->GetPos();
        --new_pos.y;

        if (this->CanWalk(new_pos))
        {
            player->SetPos(new_pos);
            engine_.ComputeFOV();
        }
    }

    void MoveEventHandler::Handle(MoveDownEvent&)
    {
        auto* player = engine_.GetPlayer();
        auto new_pos = player->GetPos();
        ++new_pos.y;

        if (this->CanWalk(new_pos))
        {
            player->SetPos(new_pos);
            engine_.ComputeFOV();
        }
    }

    void MoveEventHandler::Handle(MoveLeftEvent&)
    {
        auto* player = engine_.GetPlayer();
        auto new_pos = player->GetPos();
        --new_pos.x;

        if (this->CanWalk(new_pos))
        {
            player->SetPos(new_pos);
            engine_.ComputeFOV();
        }
    }

    void MoveEventHandler::Handle(MoveRightEvent&)
    {
        auto* player = engine_.GetPlayer();
        auto new_pos = player->GetPos();
        ++new_pos.x;

        if (this->CanWalk(new_pos))
        {
            player->SetPos(new_pos);
            engine_.ComputeFOV();
        }
    }

    bool MoveEventHandler::CanWalk(Position pos) const
    {
        // Check if position is out of bounds or is a wall
        const auto* map = engine_.GetMap();

        if (!map->IsInBounds(pos) || map->IsWall(pos))
        {
            return false;
        }

        // Check if position is where another entity is
        auto& entities = engine_.GetEntities();

        for (const auto& entity : entities)
        {
            if (entity.GetPos() == pos)
            {
                return false;
            }
        }

        return true;
    }
} // namespace tutorial
