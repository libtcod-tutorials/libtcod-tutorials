#include "Engine.hpp"

#include "AnEvent.hpp"
#include "Configuration.hpp"
#include "EngineEvent.hpp"
#include "Entity.hpp"
#include "EventHandler.hpp"
#include "Map.hpp"
#include "Rectangle.hpp"
#include "Renderer.hpp"
#include "SDLEventHandler.hpp"

#include <memory>

namespace tutorial
{
    Engine::Engine(const Configuration& config)
        : renderer_(config), map_(nullptr), player_(nullptr), running_(true)
    {
        event_handler_.AddHandler(GenerateMapEventHandler { *this });
        event_handler_.AddHandler(MoveEventHandler { *this });
        event_handler_.AddHandler(QuitEventHandler { *this });

        constexpr int ui_height = 5;

        map_ = std::make_unique<Map>(Rectangle {
            config.window_size.width, config.window_size.height - ui_height });

        GenerateMapEvent event {};
        event_handler_.Handle(event);
    }

    void Engine::ComputeFOV()
    {
        constexpr int fov_radius = 10;

        map_->ComputeFov(player_->GetPos(), fov_radius);
        map_->Update();
    }

    void Engine::HandleEvents()
    {
        SDLEventHandler handler {};
        const auto event = handler.HandleInput();

        if (event)
        {
            event_handler_.Handle(*event);
        }
    }

    void Engine::Render()
    {
        renderer_.Render(*map_.get(), entities_);
    }

    void Engine::SetMap(Map&& map)
    {
        map_ = std::make_unique<Map>(std::move(map));
    }

    void Engine::SetPlayer(Entity* entity)
    {
        player_ = entity;
    }

    void Engine::Quit()
    {
        running_ = false;
    }

    std::vector<Entity>& Engine::GetEntities()
    {
        return entities_;
    }

    Map* Engine::GetMap() const
    {
        return map_.get();
    }

    Entity* Engine::GetPlayer() const
    {
        return player_;
    }

    bool Engine::IsRunning() const
    {
        return running_;
    }
} // namespace tutorial
