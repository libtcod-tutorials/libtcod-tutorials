#include "Renderer.hpp"

#include "Configuration.hpp"
#include "Entity.hpp"
#include "Rectangle.hpp"

#include <SDL2/SDL_video.h>
#include <libtcod/context.h>
#include <libtcod/version.h>

#include <algorithm>

namespace tutorial
{
    Renderer::Renderer(const Configuration& config)
        : console_(config.window_size.width, config.window_size.height)
    {
        TCOD_ContextParams params {};
        params.tcod_version = TCOD_COMPILEDVERSION;
        params.console = console_.get();
        params.window_title = config.window_title.data();
        params.sdl_window_flags = SDL_WINDOW_RESIZABLE;

        context_ = tcod::Context { params };
    }

    void Renderer::Render(const Map& map, const std::vector<Entity>& entities)
    {
        console_.clear();

        this->RenderMap(map);
        this->RenderEntities(entities);

        context_.present(console_);
    }

    void Renderer::RenderEntities(const std::vector<Entity>& entities)
    {
        // Show the position of each entity
        std::for_each(entities.cbegin(), entities.cend(),
                      [&](const auto entity) { entity.Render(console_); });
    }

    void Renderer::RenderMap(const Map& map)
    {
        map.Render(console_);
    }
} // namespace tutorial
