#include "Renderable.hpp"

#include "Position.hpp"

#include <libtcod/console_types.hpp>

namespace tutorial
{
    Renderable::Renderable(tcod::ColorRGB color, char icon)
        : color_(color), icon_(icon)
    {
    }

    void Renderable::Render(tcod::Console& console, Position pos) const
    {
        console.at({ pos.x, pos.y }).ch = icon_;
        console.at({ pos.x, pos.y }).fg = color_;
    }
} // namespace tutorial
