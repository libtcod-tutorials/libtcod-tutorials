# Set the cmake minimum version
cmake_minimum_required(VERSION 3.30)

# Set the default toolchain to use a Vcpkg submodule.
if(NOT DEFINED CMAKE_TOOLCHAIN_FILE)
    set(CMAKE_TOOLCHAIN_FILE
        "${CMAKE_CURRENT_SOURCE_DIR}/vcpkg/scripts/buildsystems/vcpkg.cmake"
        CACHE STRING "Vcpkg toolchain file")
endif()

# Name of the CMake project, description, and what languages it uses
project(LibtcodTutorials 
    DESCRIPTION "Modern C++ libtcod tutorials"
    LANGUAGES CXX
)

# Use C++23 standard for compiling
set(CMAKE_CXX_STANDARD 23)

# Add more warnings to the default compiler flags
if(MSVC)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W4")
else()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pedantic -Wall -Wextra")
endif()

find_package(fmt CONFIG REQUIRED)
find_package(SDL2 CONFIG REQUIRED)
find_package(libtcod CONFIG REQUIRED)

## Add the subdirectories containing the tutorials
add_subdirectory(tutorial0)
add_subdirectory(tutorial1)
add_subdirectory(tutorial2)
add_subdirectory(tutorial3)
add_subdirectory(tutorial4)
add_subdirectory(tutorial5)
add_subdirectory(tutorial6)
add_subdirectory(tutorial7)
add_subdirectory(tutorial8)
