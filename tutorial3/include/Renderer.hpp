#ifndef RENDERER_HPP
#define RENDERER_HPP

#include <libtcod/console_types.hpp>
#include <libtcod/context.hpp>

#include <vector>

namespace tutorial
{
    class Entity;
    class Map;
    struct Configuration;

    class Renderer
    {
    public:
        Renderer(const Configuration& config);

        void Render(const Map& map, const std::vector<Entity>& entities);

    private:
        void RenderEntities(const std::vector<Entity>& entities);
        void RenderMap(const Map& map);

        tcod::Console console_;
        tcod::Context context_;
    };
} // namespace tutorial

#endif // RENDERER_HPP
