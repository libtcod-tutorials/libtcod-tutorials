#ifndef EVENT_HPP
#define EVENT_HPP

namespace tutorial
{
    enum class Event
    {
        None,
        MoveUp,
        MoveDown,
        MoveLeft,
        MoveRight,
        Quit,
        GenMap,
    };
}

#endif // EVENT_HPP
