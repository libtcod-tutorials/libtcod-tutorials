#ifndef ENGINE_HPP
#define ENGINE_HPP

#include "Entity.hpp"
#include "Map.hpp"
#include "Rectangle.hpp"
#include "Renderer.hpp"

#include <SDL2/SDL_events.h>

#include <memory>
#include <vector>

namespace tutorial
{
    enum class Event : int;
    struct Configuration;
    struct Position;

    class Engine
    {
    public:
        Engine(const Configuration& config);

        void Input();
        void Render();
        void Update();

        bool IsRunning() const;

    private:
        void GenerateMap();
        void HandleKeydownEvent(const SDL_Event& event);
        void Quit();

        bool CanMove(Position pos) const;

        std::vector<Entity> entities_;

        Renderer renderer_;

        std::unique_ptr<Map> map_;

        Rectangle screen_size_;

        Entity* player_;

        Event next_event_;

        bool running_;
    };
} // namespace tutorial

#endif // ENGINE_HPP
