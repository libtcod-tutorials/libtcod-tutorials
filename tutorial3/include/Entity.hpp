#ifndef ENTITY_HPP
#define ENTITY_HPP

#include "Position.hpp"

#include <libtcod/color.hpp>

namespace tutorial
{
    class Entity
    {
    public:
        explicit Entity(Position pos, TCOD_ColorRGB color)
            : pos_(pos), color_(color)
        {
        }

        constexpr void SetPos(Position pos)
        {
            pos_ = pos;
        }

        constexpr TCOD_ColorRGB GetColor() const
        {
            return color_;
        }

        constexpr Position GetPos() const
        {
            return pos_;
        }

    private:
        Position pos_;
        TCOD_ColorRGB color_;
    };
} // namespace tutorial

#endif // ENTITY_HPP
