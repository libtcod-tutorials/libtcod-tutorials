#ifndef ROOM_HPP
#define ROOM_HPP

#include "Position.hpp"

#include <vector>

namespace tutorial
{
    class Room
    {
    public:
        Room(Position origin, Position end) : origin_(origin), end_(end)
        {
        }

        constexpr Position GetCenter() const
        {
            return ((origin_ + end_) / 2);
        }

        std::vector<Position> GetInner() const;

        constexpr bool Intersects(const Room& other) const
        {
            return (origin_.x <= other.end_.x && end_.x >= other.origin_.x
                    && origin_.y <= other.end_.y && end_.y >= other.origin_.y);
        }

    private:
        Position origin_;
        Position end_;
    };
} // namespace tutorial

#endif // ROOM_HPP
