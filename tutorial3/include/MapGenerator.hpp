#ifndef MAP_GENERATOR_HPP
#define MAP_GENERATOR_HPP

#include "Room.hpp"

#include <vector>

namespace tutorial
{
    enum class Tile : int;
    struct Rectangle;

    class MapGenerator
    {
    public:
        void Generate(std::vector<Tile>& tiles, Rectangle size);

        constexpr const std::vector<Room>& GetRooms() const
        {
            return rooms_;
        }

    private:
        std::vector<Room> rooms_;
    };
} // namespace tutorial

#endif // MAP_GENERATOR_HPP
