#ifndef UTILITY_HPP
#define UTILITY_HPP

#include "Position.hpp"

#include <cstdint>

namespace tutorial
{
    namespace util
    {
        constexpr Position indexToPos(std::size_t i, int width)
        {
            const int y = i / width;
            const int x = i - y * width;

            return Position { x, y };
        }

        constexpr std::size_t posToIndex(Position pos, int width)
        {
            return (pos.x + pos.y * width);
        }

    } // namespace util
} // namespace tutorial

#endif // UTILITY_HPP
