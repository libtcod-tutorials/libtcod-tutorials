#ifndef COLORS_HPP
#define COLORS_HPP

#include <libtcod/color.hpp>

namespace tutorial::color
{
    constexpr TCOD_ColorRGB Black { 0, 0, 0 };
    constexpr TCOD_ColorRGB Yellow { 255, 255, 0 };
    constexpr TCOD_ColorRGB White { 255, 255, 255 };
    constexpr TCOD_ColorRGB LightAzure { 0, 95, 191 };
    constexpr TCOD_ColorRGB DarkAzure { 63, 59, 155 };
} // namespace tutorial::color

#endif // COLORS_HPP
