#ifndef MAP_HPP
#define MAP_HPP

#include "Rectangle.hpp"
#include "Room.hpp"

#include <libtcod/console_types.hpp>

#include <vector>

namespace tutorial
{
    class MapGenerator;
    enum class Tile : int;
    struct Position;

    class Map
    {
    public:
        explicit Map(MapGenerator& generator, Rectangle size);

        bool CanWalk(Position pos) const;

        const std::vector<Room>& GetRooms() const
        {
            return rooms_;
        }

        constexpr Rectangle GetSize() const
        {
            return size_;
        }

        void Render(tcod::Console& console) const;

    private:
        void Generate(MapGenerator& generator);
        void Render();
        void SetWall(Position pos);

        std::vector<Tile> tiles_;
        std::vector<Room> rooms_;

        tcod::Console console_;

        Rectangle size_;
    };
} // namespace tutorial

#endif // MAP_HPP
