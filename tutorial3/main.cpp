#include "Configuration.hpp"
#include "Engine.hpp"
#include "Rectangle.hpp"

#include <string_view>

int main()
{
    constexpr int window_width = 80;
    constexpr int window_height = 50;
    const std::string_view window_title = "libtcod C++ tutorial 3";

    const tutorial::Configuration config {
        window_title, tutorial::Rectangle { window_width, window_height }
    };

    tutorial::Engine engine { config };

    while (engine.IsRunning())
    {
        engine.Render();
        engine.Input();
        engine.Update();
    }

    return 0;
}
