#include "Room.hpp"

namespace tutorial
{
    std::vector<Position> Room::GetInner() const
    {
        std::vector<Position> inner {};

        for (int x = origin_.x + 1; x < end_.x; ++x)
        {
            for (int y = origin_.y + 1; y < end_.y; ++y)
            {
                inner.push_back(Position { x, y });
            }
        }

        return inner;
    }
} // namespace tutorial
