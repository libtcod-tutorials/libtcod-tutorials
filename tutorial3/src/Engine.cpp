#include "Engine.hpp"

#include "Colors.hpp"
#include "Configuration.hpp"
#include "Event.hpp"
#include "MapGenerator.hpp"
#include "Position.hpp"
#include "Renderer.hpp"
#include "Room.hpp"

#include <SDL2/SDL_events.h>
#include <SDL2/SDL_keycode.h>

#include <algorithm>
#include <cstddef>
#include <memory>

namespace tutorial
{
    Engine::Engine(const Configuration& config)
        : renderer_(config), screen_size_(config.window_size), running_(true)
    {
        this->GenerateMap();
    }

    void Engine::Input()
    {
        SDL_Event event {};

        SDL_WaitEvent(&event);

        switch (event.type)
        {
            case SDL_QUIT:
                next_event_ = Event::Quit;
                break;
            case SDL_KEYDOWN:
                HandleKeydownEvent(event);
                break;
            default:
                next_event_ = Event::None;
                break;
        }

        SDL_FlushEvent(SDL_KEYDOWN);
    }

    void Engine::Render()
    {
        renderer_.Render(*map_.get(), entities_);
    }

    void Engine::Update()
    {
        if (next_event_ == Event::None)
        {
            return;
        }

        if (next_event_ == Event::Quit)
        {
            Quit();
            return;
        }

        if (next_event_ == Event::GenMap)
        {
            this->GenerateMap();
            return;
        }

        auto pos = player_->GetPos();

        switch (next_event_)
        {
            case Event::MoveUp:
                --pos.y;
                break;
            case Event::MoveDown:
                ++pos.y;
                break;
            case Event::MoveLeft:
                --pos.x;
                break;
            case Event::MoveRight:
                ++pos.x;
                break;
            default:
                break;
        }

        if (this->CanMove(pos))
        {
            player_->SetPos(pos);
        }

        next_event_ = Event::None;
    }

    bool Engine::IsRunning() const
    {
        return running_;
    }

    void Engine::GenerateMap()
    {
        player_ = nullptr;
        entities_.clear();

        MapGenerator generator {};

        map_ = std::make_unique<Map>(generator, screen_size_);

        const auto& rooms = map_->GetRooms();

        entities_.emplace_back(rooms[0].GetCenter(), color::White);

        for (std::size_t i = 1; i < rooms.size(); ++i)
        {
            entities_.emplace_back(rooms[i].GetCenter(), color::Yellow);
        }

        player_ = &(entities_[0]);
    }

    void Engine::HandleKeydownEvent(const SDL_Event& event)
    {
        switch (event.key.keysym.sym)
        {
            case SDLK_UP:
                next_event_ = Event::MoveUp;
                break;
            case SDLK_DOWN:
                next_event_ = Event::MoveDown;
                break;
            case SDLK_LEFT:
                next_event_ = Event::MoveLeft;
                break;
            case SDLK_RIGHT:
                next_event_ = Event::MoveRight;
                break;
            case SDLK_RETURN:
                next_event_ = Event::GenMap;
                break;
            case SDLK_ESCAPE:
                next_event_ = Event::Quit;
                break;
        }
    }

    void Engine::Quit()
    {
        running_ = false;
    }

    bool Engine::CanMove(Position pos) const
    {
        if (!map_->CanWalk(pos))
        {
            return false;
        }

        for (auto entity : entities_)
        {
            if (pos == entity.GetPos())
            {
                return false;
            }
        }

        return true;
    }
} // namespace tutorial
