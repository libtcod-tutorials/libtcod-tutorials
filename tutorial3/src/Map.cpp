#include "Map.hpp"

#include "Colors.hpp"
#include "MapGenerator.hpp"
#include "Position.hpp"
#include "Rectangle.hpp"
#include "Tile.hpp"
#include "Utility.hpp"

#include <libtcod/color.h>
#include <libtcod/console.hpp>

#include <cstddef>

namespace tutorial
{
    Map::Map(MapGenerator& generator, Rectangle size)
        : tiles_(size.width * size.height, Tile::Wall),
          console_(size.width, size.height),
          size_(size)
    {
        this->Generate(generator);
    }

    bool Map::CanWalk(Position pos) const
    {
        const auto index = util::posToIndex(pos, size_.width);

        return tiles_.at(index) == Tile::Floor;
    }

    void Map::Render(tcod::Console& parent) const
    {
        tcod::blit(parent, console_);
    }

    void Map::Generate(MapGenerator& generator)
    {
        generator.Generate(tiles_, size_);
        rooms_ = generator.GetRooms();

        this->Render();
    }

    void Map::Render()
    {
        for (std::size_t i = 0; i < tiles_.size(); ++i)
        {
            const auto pos = util::indexToPos(i, size_.width);

            TCOD_ColorRGB color = color::Black;

            switch (tiles_.at(i))
            {
                case Tile::Floor:
                    color = color::LightAzure;
                    break;
                case Tile::Wall:
                    color = color::DarkAzure;
                    break;
            }

            console_.at({ pos.x, pos.y }).bg = color;
        }
    }
} // namespace tutorial
