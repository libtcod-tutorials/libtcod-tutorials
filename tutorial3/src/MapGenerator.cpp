#include "MapGenerator.hpp"

#include "Position.hpp"
#include "Rectangle.hpp"
#include "Room.hpp"
#include "Tile.hpp"
#include "Utility.hpp"

#include <libtcod/bresenham.hpp>
#include <libtcod/mersenne.hpp>

#include <algorithm>

namespace tutorial
{
    namespace
    {
        constexpr float half_chance = 0.5F;

        std::vector<Position> tunnelBetween(Position start, Position end)
        {
            auto* rand = TCODRandom::getInstance();

            Position corner { 0, 0 };

            if (rand->get(0.0F, 1.0F) < half_chance)
            {
                corner = Position { end.x, start.y };
            }
            else
            {
                corner = Position { start.x, end.y };
            }

            std::vector<Position> tunnel {};

            for (const auto&& [x, y] : tcod::BresenhamLine(
                     { start.x, start.y }, { corner.x, corner.y }))
            {
                tunnel.push_back({ x, y });
            }

            for (const auto&& [x, y] :
                 tcod::BresenhamLine({ corner.x, corner.y }, { end.x, end.y }))
            {
                tunnel.push_back({ x, y });
            }

            return tunnel;
        }

        constexpr int room_min_size = 6;
        constexpr int room_max_size = 10;
        constexpr int max_rooms = 30;

        void digRooms(std::vector<Room>& rooms, std::vector<Tile>& tiles,
                      int width, int height)
        {
            auto* rand = TCODRandom::getInstance();

            for (int i = 0; i < max_rooms; ++i)
            {
                int room_width = rand->getInt(room_min_size, room_max_size);
                int room_height = rand->getInt(room_min_size, room_max_size);

                Position room_origin { rand->getInt(0, width - room_width - 1),
                                       rand->getInt(0,
                                                    height - room_height - 1) };

                Position room_end { room_origin.x + room_width,
                                    room_origin.y + room_height };

                Room room { room_origin, room_end };

                auto it = std::find_if(rooms.begin(), rooms.end(),
                                       [&room](Room other)
                                       { return room.Intersects(other); });

                if (it != rooms.end())
                {
                    continue;
                }

                for (auto pos : room.GetInner())
                {
                    const auto index = util::posToIndex(pos, width);

                    tiles.at(index) = Tile::Floor;
                }

                rooms.push_back(room);
            }
        }

        void digTunnels(const std::vector<Room>& rooms,
                        std::vector<Tile>& tiles, int width)
        {
            for (const auto& room : rooms)
            {
                auto tunnel = tunnelBetween(
                    room.GetCenter(), rooms.at(rooms.size() - 1).GetCenter());

                for (auto pos : tunnel)
                {
                    const auto index = util::posToIndex(pos, width);

                    tiles.at(index) = Tile::Floor;
                }
            }
        }
    } // namespace

    void MapGenerator::Generate(std::vector<Tile>& tiles, Rectangle size)
    {
        digRooms(rooms_, tiles, size.width, size.height);
        digTunnels(rooms_, tiles, size.width);
    }
} // namespace tutorial
