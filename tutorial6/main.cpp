#include "Engine.hpp"

#include <string>

int main()
{
    constexpr int WINDOW_WIDTH = 80;
    constexpr int WINDOW_HEIGHT = 50;
    static const std::string WINDOW_TITLE = "libtcod C++ tutorial 6";

    tutorial::Engine engine { WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_TITLE };

    while (engine.IsRunning())
    {
        engine.Render();
        engine.HandleEvents();
    }

    return 0;
}
