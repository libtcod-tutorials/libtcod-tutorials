#ifndef ENTITY_HPP
#define ENTITY_HPP

#include "Components.hpp"
#include "Position.hpp"
#include "Renderable.hpp"

#include <string>

namespace tutorial
{
    class Engine;

    class Entity
    {
    public:
        using uint = unsigned int;

        Entity(pos_t pos, const std::string& name, bool blocker,
               AttackComponent attack, const DefenseComponent& defense,
               const CharRenderable& renderable);

        void Act(Engine& engine);
        void Die();
        void SetPos(pos_t pos);
        void TakeDamage(uint value);

        uint Attack() const;
        uint Defend() const;
        const std::string& GetName() const;
        pos_t GetPos() const;
        bool IsBlocker() const;
        bool IsDead() const;
        void Render(TCODConsole* console) const;

        bool operator==(const Entity& rhs);

    private:
        std::string name_;
        CharRenderable renderable_;
        DefenseComponent defense_;
        pos_t pos_;
        AttackComponent attack_;
        bool blocker_;
    };

    static const Entity PLAYER { pos_t { 0, 0 },
                                 "player",
                                 true,
                                 AttackComponent { 5 },
                                 DefenseComponent { 2, 30 },
                                 CharRenderable { TCODColor::white, '@' }

    };

    static const Entity ORC {
        pos_t { 0, 0 },
        "orc",
        true,
        AttackComponent { 3 },
        DefenseComponent { 0, 10 },
        CharRenderable { TCODColor::desaturatedGreen, 'o' }

    };

    static const Entity TROLL { pos_t { 0, 0 },
                                "troll",
                                true,
                                AttackComponent { 4 },
                                DefenseComponent { 1, 16 },
                                CharRenderable { TCODColor::darkerGreen, 'T' }

    };
} // namespace tutorial

#endif // ENTITY_HPP
