#ifndef COMPONENTS_HPP
#define COMPONENTS_HPP

namespace tutorial
{
    struct AttackComponent
    {
        unsigned int power;
    };

    struct DefenseComponent
    {
        DefenseComponent(unsigned int defense, unsigned int maxHp)
            : defense(defense), hp(maxHp), maxHp(maxHp)
        {
        }

        unsigned int defense;
        unsigned int hp;
        unsigned int maxHp;
    };
} // namespace tutorial

#endif // COMPONENTS_HPP
