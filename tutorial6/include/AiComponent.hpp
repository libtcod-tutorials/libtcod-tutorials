#ifndef AI_COMPONENT_HPP
#define AI_COMPONENT_HPP

#include "Position.hpp"

#include <libtcod/bresenham.hpp>

#include <memory>

namespace tutorial
{
    class Engine;
    class Entity;

    class AiComponent
    {
    public:
        virtual ~AiComponent() = default;

        virtual void Perform(Engine& engine) = 0;
    };

    class BaseAi : public AiComponent
    {
    public:
        BaseAi(Entity& entity);

        virtual void Perform(Engine& engine) override;

    protected:
        Entity& entity_;
    };

    class HostileAi final : public BaseAi
    {
    public:
        HostileAi(Entity& entity);

        void Perform(Engine& engine) override;
    };
} // namespace tutorial

#endif // AI_COMPONENT_HPP
