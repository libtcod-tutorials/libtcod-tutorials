#ifndef COMBAT_SYSTEM_HPP
#define COMBAT_SYSTEM_HPP

#include "Entity.hpp"

namespace tutorial::combat
{
    auto handleAttack = [](const Entity& attacker, Entity& target) -> long
    {
        auto damage = long(attacker.Attack()) - target.Defend();

        if (damage > 0)
        {
            target.TakeDamage(damage);

            return damage;
        }

        return 0;
    };
} // namespace tutorial::combat

#endif // COMBAT_SYSTEM_HPP
