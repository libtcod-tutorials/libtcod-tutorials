#ifndef ENGINE_HPP
#define ENGINE_HPP

#include "EntityManager.hpp"
#include "Position.hpp"

#include <memory>
#include <queue>
#include <string>
#include <vector>

namespace tutorial
{
    class Entity;
    class EventHandler;
    class Map;

    class Engine
    {
    public:
        Engine(int width, int height, std::string title);
        ~Engine();

        void ComputeFOV();
        void GenerateMap();
        Entity* GetBlockingEntity(pos_t pos);
        Entity* GetPlayer();
        void HandleDeathEvent();
        void HandleEvents();
        void Quit();

        bool IsBlocker(pos_t pos) const;
        bool IsInBounds(pos_t pos) const;
        bool IsInFov(pos_t pos) const;
        bool IsPlayer(const Entity& entity) const;
        bool IsRunning() const;
        bool IsWall(pos_t pos) const;
        void Render() const;

    private:
        void GenerateMap(int width, int height);
        void HandleEnemyTurns();

        EntityManager entities_;

        std::unique_ptr<EventHandler> eventHandler_;
        std::unique_ptr<Map> map_;

        Entity* player_;

        bool running_;
    };
} // namespace tutorial

#endif // ENGINE_HPP
