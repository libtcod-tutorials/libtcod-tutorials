#ifndef ENTITY_MANAGER_HPP
#define ENTITY_MANAGER_HPP

#include "Entity.hpp"
#include "Position.hpp"
#include "Room.hpp"

#include <vector>

namespace tutorial
{
    class EntityManager
    {
    public:
        void Clear();
        Entity* GetBlockingEntity(pos_t pos);
        void PlaceEntities(const Room& room, int maxMonstersPerRoom);
        Entity& Spawn(const Entity& src);
        Entity& Spawn(const Entity& src, pos_t pos);

        using iterator = std::vector<Entity>::iterator;
        using const_iterator = std::vector<Entity>::const_iterator;

        iterator begin()
        {
            return entities_.begin();
        }

        iterator end()
        {
            return entities_.end();
        }

        const_iterator begin() const
        {
            return entities_.begin();
        }

        const_iterator end() const
        {
            return entities_.end();
        }

    private:
        std::vector<Entity> entities_;
    };
} // namespace tutorial

#endif // ENTITY_MANAGER_HPP
