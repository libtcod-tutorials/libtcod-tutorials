#ifndef MAP_HPP
#define MAP_HPP

#include "Position.hpp"
#include "Room.hpp"
#include "Tile.hpp"

#include "libtcod/console.hpp"
#include "libtcod/fov.hpp"

#include <memory>
#include <vector>

namespace tutorial
{
    class Entity;
    class EntityManager;

    class Map
    {
    public:
        class Generator;

        Map(int width, int height);

        void ComputeFov(pos_t origin, int fovRadius);
        void SetExplored(pos_t pos, bool explored);
        void SetTileType(pos_t pos, TileType type);
        void Update();

        int GetHeight() const;
        const std::vector<Room>& GetRooms() const;
        TileType GetTileType(pos_t pos) const;
        int GetWidth() const;
        bool IsExplored(pos_t pos) const;
        bool IsInBounds(pos_t pos) const;
        bool IsInFov(pos_t pos) const;
        bool IsWall(pos_t pos) const;
        void Render(TCODConsole* parent) const;

    private:
        int width_;
        int height_;

        std::vector<Room> rooms_;
        std::vector<tile_t> tiles_;

        std::unique_ptr<TCODConsole> console_;
        std::unique_ptr<TCODMap> map_;
    };
} // namespace tutorial

#endif // MAP_HPP
