#ifndef EVENT_HANDLER_HPP
#define EVENT_HANDLER_HPP

#include "Event.hpp"

#include <memory>

namespace tutorial
{
    class Engine;

    class EventHandler
    {
    public:
        virtual ~EventHandler() = default;

        virtual std::unique_ptr<Event> Dispatch() const = 0;
    };

    class BaseEventHandler : public EventHandler
    {
    public:
        BaseEventHandler(Engine& engine) : engine_(engine)
        {
        }

        virtual std::unique_ptr<Event> Dispatch() const = 0;

    protected:
        Engine& engine_;
    };

    class MainGameEventHandler final : public BaseEventHandler
    {
    public:
        MainGameEventHandler(Engine& engine) : BaseEventHandler(engine)
        {
        }

        std::unique_ptr<Event> Dispatch() const override;
    };

    class GameOverEventHandler final : public BaseEventHandler
    {
    public:
        GameOverEventHandler(Engine& engine) : BaseEventHandler(engine)
        {
        }

        std::unique_ptr<Event> Dispatch() const override;
    };
} // namespace tutorial

#endif // EVENT_HANDLER_HPP
