#ifndef POSITION_HPP
#define POSITION_HPP

namespace tutorial
{
    struct pos_t
    {
        int x;
        int y;
    };

    bool operator==(pos_t lhs, pos_t rhs);
    bool operator!=(pos_t lhs, pos_t rhs);

    pos_t operator+(pos_t lhs, pos_t rhs);
    void operator+=(pos_t& lhs, pos_t rhs);
    pos_t operator-(pos_t lhs, pos_t rhs);
    pos_t operator/(pos_t lhs, int rhs);
} // namespace tutorial

#endif // POSITION_HPP
