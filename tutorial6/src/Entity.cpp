#include "Entity.hpp"

#include "AiComponent.hpp"
#include "Engine.hpp"
#include "Util.hpp"

#include <algorithm>

namespace tutorial
{
    static const CharRenderable kDeadRenderable { TCODColor(191, 0, 0), '%' };

    Entity::Entity(pos_t pos, const std::string& name, bool blocker,
                   AttackComponent attack, const DefenseComponent& defense,
                   const CharRenderable& renderable)
        : name_(name),
          renderable_(renderable),
          defense_(defense),
          pos_(pos),
          attack_(attack),
          blocker_(blocker)
    {
    }

    void Entity::Act(Engine& engine)
    {
        if (!engine.IsPlayer(*this))
        {
            auto ai = HostileAi(*this);
            ai.Perform(engine);
        }
    }

    void Entity::Die()
    {
        renderable_ = kDeadRenderable;
        blocker_ = false;
        name_ = "remains of " + name_;
    }

    void Entity::SetPos(pos_t pos)
    {
        pos_ = pos;
    }

    void Entity::TakeDamage(uint value)
    {
        defense_.hp -= std::max<uint>(0, std::min<uint>(value, defense_.maxHp));
    }

    Entity::uint Entity::Attack() const
    {
        return attack_.power;
    }

    Entity::uint Entity::Defend() const
    {
        return defense_.defense;
    }

    const std::string& Entity::GetName() const
    {
        return name_;
    }

    pos_t Entity::GetPos() const
    {
        return pos_;
    }

    bool Entity::IsBlocker() const
    {
        return blocker_;
    }

    bool Entity::IsDead() const
    {
        return (defense_.hp <= 0);
    }

    void Entity::Render(TCODConsole* console) const
    {
        renderable_.Render(console, pos_);
    }
} // namespace tutorial
