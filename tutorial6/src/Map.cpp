#include "Map.hpp"

#include "Entity.hpp"
#include "EntityManager.hpp"
#include "Tile.hpp"
#include "Util.hpp"

#include <memory>

namespace tutorial
{
    Map::Map(int width, int height)
        : width_(width),
          height_(height),
          tiles_(std::vector<tile_t>(width * height,
                                     tile_t { false, TileType::WALL })),
          console_(std::make_unique<TCODConsole>(width, height)),
          map_(std::make_unique<TCODMap>(width, height))
    {
    }

    void Map::ComputeFov(pos_t origin, int fovRadius)
    {
        map_->computeFov(origin.x, origin.y, fovRadius);
    }

    void Map::SetExplored(pos_t pos, bool explored)
    {
        tiles_.at(util::posToIndex(pos, width_)).explored = explored;
    }

    void Map::SetTileType(pos_t pos, TileType type)
    {
        tiles_.at(util::posToIndex(pos, width_)).type = type;

        switch (type)
        {
            case TileType::FLOOR:
                map_->setProperties(pos.x, pos.y, true, true);
                break;
            case TileType::WALL:
                map_->setProperties(pos.x, pos.y, false, false);
                break;
            default:
                break;
        }
    }

    void Map::Update()
    {
        const TCODColor DARK_GROUND = TCODColor::lightAzure;
        const TCODColor DARK_WALL = TCODColor::darkAzure;
        const TCODColor LIGHT_GROUND = TCODColor::lightAmber;
        const TCODColor LIGHT_WALL = TCODColor::darkAmber;

        console_->clear();

        for (int x = 0; x < width_; ++x)
        {
            for (int y = 0; y < height_; ++y)
            {
                TCODColor color {};
                auto& tile = tiles_.at(util::posToIndex({ x, y }, width_));

                if (this->IsInFov({ x, y }))
                {
                    tile.explored = true;

                    switch (tile.type)
                    {
                        case TileType::FLOOR:
                            color = LIGHT_GROUND;
                            break;
                        case TileType::WALL:
                            color = LIGHT_WALL;
                            break;
                        default:
                            break;
                    }
                }
                else if (this->IsExplored({ x, y }))
                {
                    switch (tile.type)
                    {
                        case TileType::FLOOR:
                            color = DARK_GROUND;
                            break;
                        case TileType::WALL:
                            color = DARK_WALL;
                            break;
                        default:
                            break;
                    }
                }

                console_->setCharBackground(x, y, color);
            }
        }

        console_->flush();
    }

    int Map::GetHeight() const
    {
        return height_;
    }

    const std::vector<Room>& Map::GetRooms() const
    {
        return rooms_;
    }

    TileType Map::GetTileType(pos_t pos) const
    {
        return tiles_.at(util::posToIndex(pos, width_)).type;
    }

    int Map::GetWidth() const
    {
        return width_;
    }

    bool Map::IsInBounds(pos_t pos) const
    {
        return (pos.x >= 0 && pos.y >= 0 && pos.x < width_ && pos.y < height_);
    }

    bool Map::IsExplored(pos_t pos) const
    {
        return tiles_.at(util::posToIndex(pos, width_)).explored;
    }

    bool Map::IsInFov(pos_t pos) const
    {
        return map_->isInFov(pos.x, pos.y);
    }

    bool Map::IsWall(pos_t pos) const
    {
        return !map_->isWalkable(pos.x, pos.y);
    }

    void Map::Render(TCODConsole* parent) const
    {
        TCODConsole::blit(console_.get(), 0, 0, width_, height_, parent, 0, 0);
    }
} // namespace tutorial
