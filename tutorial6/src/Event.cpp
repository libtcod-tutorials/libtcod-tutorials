#include "Event.hpp"

#include "CombatSystem.hpp"
#include "Engine.hpp"
#include "Entity.hpp"
#include "Util.hpp"

#include <fmt/base.h>

namespace tutorial
{
    GenerateMapEvent::GenerateMapEvent(Engine& engine) : EngineEvent(engine)
    {
    }

    void GenerateMapEvent::Execute()
    {
        engine_.GenerateMap();
    }

    QuitEvent::QuitEvent(Engine& engine) : EngineEvent(engine)
    {
    }

    void QuitEvent::Execute()
    {
        engine_.Quit();
    }

    DieAction::DieAction(Engine& engine, Entity& entity)
        : Action(engine, entity)
    {
    }

    void DieAction::Execute()
    {
        if (engine_.IsPlayer(entity_))
        {
            fmt::println("You died!");
            engine_.HandleDeathEvent();
        }
        else
        {
            fmt::println("{} has died!", util::capitalize(entity_.GetName()));
        }

        entity_.Die();
    }

    WaitAction::WaitAction(Engine& engine, Entity& entity)
        : Action(engine, entity)
    {
    }

    void WaitAction::Execute()
    {
    }

    BumpAction::BumpAction(Engine& engine, Entity& entity, pos_t pos)
        : DirectionalAction(engine, entity, pos)
    {
    }

    void BumpAction::Execute()
    {
        auto targetPos = entity_.GetPos() + pos_;

        if (engine_.GetBlockingEntity(targetPos))
        {
            auto action = MeleeAction(engine_, entity_, pos_);
            action.Execute();
        }
        else
        {
            auto action = MoveAction(engine_, entity_, pos_);
            action.Execute();
        }
    }

    MeleeAction::MeleeAction(Engine& engine, Entity& entity, pos_t pos)
        : DirectionalAction(engine, entity, pos)
    {
    }

    void MeleeAction::Execute()
    {
        auto targetPos = entity_.GetPos() + pos_;
        auto* target = engine_.GetBlockingEntity(targetPos);

        if (target)
        {
            auto damage = combat::handleAttack(entity_, *target);

            if (damage > 0)
            {
                fmt::println("{} attacks {} for {} hit points.",
                             util::capitalize(entity_.GetName()),
                             target->GetName(), damage);

                if (target->IsDead())
                {
                    auto action = DieAction(engine_, *target);
                    action.Execute();
                }
            }
            else
            {
                fmt::println("{} attacks {} but does no damage.",
                             util::capitalize(entity_.GetName()),
                             target->GetName());
            }
        }
    }

    MoveAction::MoveAction(Engine& engine, Entity& entity, pos_t pos)
        : DirectionalAction(engine, entity, pos)
    {
    }

    void MoveAction::Execute()
    {
        auto targetPos = entity_.GetPos() + pos_;

        if (engine_.IsInBounds(targetPos) && !engine_.IsWall(targetPos))
        {
            auto pos = entity_.GetPos() + pos_;

            entity_.SetPos(pos);

            if (engine_.IsPlayer(entity_))
            {
                engine_.ComputeFOV();
            }
        }
    }
} // namespace tutorial
