#include "AiComponent.hpp"

#include "Engine.hpp"
#include "Entity.hpp"
#include "Event.hpp"

#include <algorithm>

namespace tutorial
{
    bool canPathToTarget(const tcod::BresenhamLine& path, const Engine& engine)
    {
        for (const auto [x, y] : path)
        {
            if (engine.IsBlocker(pos_t { x, y }))
            {
                return false;
            }
        }

        return true;
    }

    BaseAi::BaseAi(Entity& entity) : entity_(entity)
    {
    }

    void BaseAi::Perform(Engine&)
    {
        // No op
    }

    HostileAi::HostileAi(Entity& entity) : BaseAi(entity)
    {
    }

    void HostileAi::Perform(Engine& engine)
    {
        auto pos = entity_.GetPos();

        if (engine.IsInFov(pos))
        {
            auto target = engine.GetPlayer();
            auto targetPos = target->GetPos();
            auto delta = targetPos - pos;

            auto distance = std::max(std::abs(delta.x), std::abs(delta.y));

            if (distance <= 1)
            {
                auto action = MeleeAction(engine, entity_, delta);
                action.Execute();

                return;
            }

            auto path = tcod::BresenhamLine({ pos.x, pos.y },
                                            { targetPos.x, targetPos.y })
                            .without_endpoints();

            if (canPathToTarget(path, engine))
            {
                auto dest = path[0];
                auto destPos = pos_t { dest[0], dest[1] } - pos;

                auto action = MoveAction(engine, entity_, destPos);
                action.Execute();

                return;
            }

            auto action = WaitAction(engine, entity_);
            action.Execute();
        }
    }
} // namespace tutorial
