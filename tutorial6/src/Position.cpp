#include "Position.hpp"
#include <type_traits>

namespace tutorial
{
    static_assert(std::is_standard_layout<pos_t>(), "");
    static_assert(std::is_trivially_copyable<pos_t>(), "");

    bool operator==(pos_t lhs, pos_t rhs)
    {
        return (lhs.x == rhs.x && lhs.y == rhs.y);
    }

    bool operator!=(pos_t lhs, pos_t rhs)
    {
        return !(lhs == rhs);
    }

    pos_t operator+(pos_t lhs, pos_t rhs)
    {
        return pos_t { lhs.x + rhs.x, lhs.y + rhs.y };
    }

    void operator+=(pos_t& lhs, pos_t rhs)
    {
        lhs = lhs + rhs;
    }

    pos_t operator-(pos_t lhs, pos_t rhs)
    {
        return pos_t { lhs.x - rhs.x, lhs.y - rhs.y };
    }

    pos_t operator/(pos_t lhs, int rhs)
    {
        return pos_t { lhs.x / rhs, lhs.y / rhs };
    }
} // namespace tutorial
