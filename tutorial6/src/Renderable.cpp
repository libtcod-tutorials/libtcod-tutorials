#include "Renderable.hpp"

namespace tutorial
{
    void CharRenderable::Render(TCODConsole* console, pos_t pos) const
    {
        console->setChar(pos.x, pos.y, icon_);
        console->setCharForeground(pos.x, pos.y, color_);
    }
} // namespace tutorial
