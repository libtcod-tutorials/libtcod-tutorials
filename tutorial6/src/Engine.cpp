#include "Engine.hpp"

#include "Entity.hpp"
#include "EntityManager.hpp"
#include "Event.hpp"
#include "EventHandler.hpp"
#include "Map.hpp"
#include "MapGenerator.hpp"

#include "libtcod/console.hpp"
#include "libtcod/sys.hpp"

#include <iostream>
#include <memory>
#include <string>

namespace tutorial
{
    constexpr int UI_HEIGHT = 5;
    constexpr int MAX_FPS = 60;

    constexpr int ROOM_MIN_SIZE = 6;
    constexpr int ROOM_MAX_SIZE = 10;
    constexpr int MAX_ROOMS = 30;

    constexpr int FOV_RADIUS = 10;

    constexpr int MAX_MONSTERS_PER_ROOM = 3;

    // Public methods
    Engine::Engine(int width, int height, std::string title)
        : eventHandler_(new MainGameEventHandler(*this)),
          map_(nullptr),
          player_(nullptr),
          running_(true)
    {
        // Initialize the tcod root console
        TCODConsole::initRoot(width, height, title.c_str());
        TCODSystem::setFps(MAX_FPS);

        GenerateMap(width, height - UI_HEIGHT);
    }

    Engine::~Engine()
    {
        // Need to call this before we exit to clean tcod up
        TCOD_quit();
    }

    void Engine::ComputeFOV()
    {
        map_->ComputeFov(player_->GetPos(), FOV_RADIUS);
        map_->Update();
    }

    void Engine::GenerateMap()
    {
        if (map_)
        {
            GenerateMap(map_->GetWidth(), map_->GetHeight());
        }
    }

    Entity* Engine::GetBlockingEntity(pos_t pos)
    {
        return entities_.GetBlockingEntity(pos);
    }

    Entity* Engine::GetPlayer()
    {
        return player_;
    }

    void Engine::HandleDeathEvent()
    {
        eventHandler_ = std::make_unique<GameOverEventHandler>(*this);
    }

    void Engine::HandleEvents()
    {
        auto event = eventHandler_->Dispatch();

        if (event)
        {
            event->Execute();

            this->HandleEnemyTurns();
        }
    }

    void Engine::Quit()
    {
        std::exit(0);
    }

    bool Engine::IsBlocker(pos_t pos) const
    {
        for (const auto& entity : entities_)
        {
            if (entity.GetPos() == pos)
            {
                return true;
            }
        }

        return this->IsWall(pos);
    }

    bool Engine::IsInBounds(pos_t pos) const
    {
        return map_->IsInBounds(pos);
    }

    bool Engine::IsInFov(pos_t pos) const
    {
        return map_->IsInFov(pos);
    }

    bool Engine::IsPlayer(const Entity& entity) const
    {
        return player_ == &entity;
    }

    bool Engine::IsRunning() const
    {
        return (!TCODConsole::isWindowClosed() && running_);
    }

    bool Engine::IsWall(pos_t pos) const
    {
        return map_->IsWall(pos);
    }

    void Engine::Render() const
    {
        auto* root = TCODConsole::root;

        // Clear the console
        root->clear();

        // Render the map
        map_->Render(root);

        // Show all entitys positions if in fov
        for (const auto& entity : entities_)
        {
            if (map_->IsInFov(entity.GetPos()))
            {
                entity.Render(root);
            }
        }

        // Flushing the console redraws it to the screen
        TCODConsole::flush();
    }

    // Private methods
    void Engine::GenerateMap(int width, int height)
    {
        eventHandler_ = std::make_unique<MainGameEventHandler>(*this);

        entities_.Clear();

        Map::Generator generator(
            { MAX_ROOMS, ROOM_MIN_SIZE, ROOM_MAX_SIZE, width, height });

        map_ = generator.Generate();
        map_->Update();

        auto rooms = map_->GetRooms();

        // Place monsters
        for (auto it = rooms.begin() + 1; it != rooms.end(); ++it)
        {
            entities_.PlaceEntities(*it, MAX_MONSTERS_PER_ROOM);
        }

        // Create player and add them to entity list
        player_ = &entities_.Spawn(PLAYER, rooms[0].GetCenter());

        this->ComputeFOV();
    }

    void Engine::HandleEnemyTurns()
    {
        for (auto& entity : entities_)
        {
            if (this->IsPlayer(entity) || entity.IsDead())
            {
                continue;
            }

            entity.Act(*this);
        }
    }
} // namespace tutorial
