#include "EntityManager.hpp"

#include "Map.hpp"

#include <libtcod/mersenne.hpp>

namespace tutorial
{
    void EntityManager::Clear()
    {
        entities_.clear();
    }

    Entity* EntityManager::GetBlockingEntity(pos_t pos)
    {
        for (auto& entity : entities_)
        {
            if (entity.IsBlocker() && pos == entity.GetPos())
            {
                return &entity;
            }
        }

        return nullptr;
    }

    void EntityManager::PlaceEntities(const Room& room, int maxMonstersPerRoom)
    {
        auto* rand = TCODRandom::getInstance();
        int numMonsters = rand->getInt(0, maxMonstersPerRoom);

        for (int i = 0; i < numMonsters; ++i)
        {
            auto origin = room.GetOrigin();
            auto end = room.GetEnd();
            int x = rand->getInt(origin.x + 1, end.x - 1);
            int y = rand->getInt(origin.y + 1, end.y - 1);
            pos_t pos { x, y };

            // Check if a monster already exists
            if (this->GetBlockingEntity(pos))
            {
                continue;
            }

            // Add either an orc or a troll
            if (rand->getInt(0, 100) < 80)
            {
                this->Spawn(ORC, pos);
            }
            else
            {
                this->Spawn(TROLL, pos);
            }
        }
    }

    Entity& EntityManager::Spawn(const Entity& src)
    {
        return entities_.emplace_back(src);
    }

    Entity& EntityManager::Spawn(const Entity& src, pos_t pos)
    {
        auto& entity = this->Spawn(src);

        if (pos != entity.GetPos())
        {
            entity.SetPos(pos);
        }

        return entity;
    }
} // namespace tutorial
